//CSIS 2420 Dr. Grady
//Dan Taylor
//Week 6, 3
//13 Oct 2017


/* 3. Describe the web graph in detail. Also describe the space complexity using the adjacency matrix versus the adj. list to store this graph.*/

/* The web graph - Google makes billions by doing this!

Vertices - Web pages (In Google's case, a conservative estimate of 10 billion)
Edges - Links between pages (Average of 10 per website, let's say)
If we used an adjacency matrix, we would need 10 billion * 10 billion bytes (100 exabytes)
With an adjacency list, that's only 10^10 bytes (10 Gigabytes)


*/