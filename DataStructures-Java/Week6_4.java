//CSIS 2420 Dr. Grady
//Dan Taylor
//Week 6, 4
//13 Oct 2017


/* 4. Write a test driver for the Graph class we wrote in class that models a graph with 8 vertices and 12 edges.*/

public class Week6_4 {
    public static void main(String[] args) {
        Graph myGraph = new Graph();
        
        myGraph.addVertex("A");
        myGraph.addVertex("B");
        myGraph.addVertex("C");
        myGraph.addVertex("D");
        myGraph.addVertex("E");
        myGraph.addVertex("F");
        myGraph.addVertex("G");
        myGraph.addVertex("H");
        
        
        myGraph.addEdge(0,1);
        myGraph.addEdge(1,2);
        myGraph.addEdge(0,3);
        myGraph.addEdge(3,4);
        myGraph.addEdge(0,4);
        myGraph.addEdge(0,5);
        
        myGraph.displayVertices();
        myGraph.displayEdges();
    }
}