//5. Using the methods of Java’s LinkedList class, repeat week 2 ex.3.

import java.util.LinkedList;

public class Week7_5 {
    public static void main(String[] args){
		LinkedList exerciseList = new LinkedList();
		
		for (int i = 1; i <= 100; i++){
			exerciseList.addLast(i*i);
		}
		
		for (int i = 0; i < 100; i++){
			System.out.println(exerciseList.get(i));
		}
	}
}//CSIS 2420 Dr. Grady
//Dan Taylor
//Week 2, 3
//13 Sep 2017


//3. Create a list and fill it with the squares from 1 to 100, then print out the contents.
/*public class Week2_3 {
	
	
	public static void main(String[] args){
		List exerciseList = new List("Week 2 Exercise 3");
		
		for (int i = 1; i <= 100; i++){
			exerciseList.insertAtBack(i*i);
		}
		
		System.out.println(exerciseList.print());
	}
}*/