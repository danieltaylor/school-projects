//CSIS 2420 Dr. Grady
//Dan Taylor
//Week 4, 7
//29 Sep 2017

//7. Find the value that Java hashes the string “Now is the time” to.  Then 
//verify this is the same value using the code given in class.

public class Week4_7 {
    public static void main(String[] args){
        String silly = "Now is the time";
        int sillyHash = silly.hashCode();
        System.out.println("Java's version: " + sillyHash);
        
        
        int a;
        int myHash = 0;
        int sLength = silly.length();
        
        for (int i = 0; i < silly.length(); i++ ){
            a = (int)silly.charAt(i);
            //System.out.println(a);
            myHash += a*(int)(Math.pow(31,(sLength-1-i)));
        }
        
        System.out.println("In class versiion: " + myHash);
    }
}