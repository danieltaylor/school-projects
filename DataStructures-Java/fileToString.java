//Usage:java fileToString filename

import java.io.*;

public class fileToString {
    public static void main (String[] args) throws IOException {
        String data = fileToString("./awmt.txt");
        String str = cleanText(data);
        String[] wordArray = str.split(" ");
        for (String word : wordArray){
            System.out.println(word);
        }
    }
        
    public static String fileToString (String fileName) {
        String result = "";
        try {
            FileInputStream file = new FileInputStream(fileName);
            byte [] b = new byte[file.available()];
            file.read(b); //Fills the byte array with the bytes from the file. 
            file.close();
            result = new String(b); //I guess a string can be constructed from a byte array. 
        } catch (Exception e) {
            System.out.println("Meaningless errors");
        }
        return result;
    }
    
    public static String cleanText (String silly){
        silly = silly.replaceAll("\\p{Punct}", "");
        silly = silly.replaceAll("\\s+", " ");
        return silly;
    }
}