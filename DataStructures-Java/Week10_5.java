/*
CSIS 2420 Dr. Grady
Dan Taylor
Week 10, 5
8 Nov 2017

5. Write java code for computing the frequency of words in a file using the “naïve” algorithm. Then compare the times necessary to run this version versus the algorithm given in class.
*/

import java.io.*;
import java.util.*;

public class Week10_5 {
    public static void main(String[] args) throws IOException {
        String[] wordArray = utilities.fileToStringArray("./awmt.txt");
        java.util.List<String> wordList = Arrays.asList(wordArray);
        
        //Time the naive algorithm
        long startTime = System.nanoTime();
        naiveAlgorithm(wordList);    //Using the naive algorithm
        long endTime = System.nanoTime();
        long duration = (endTime - startTime);
        System.out.println("Naive algorithm execution time (ns)......" + duration);
        

        //Time the improved algorithm
        startTime = System.nanoTime();
        improvedAlgorithm(wordList);    //Using the faster algorithm
        endTime = System.nanoTime();
        duration = (endTime - startTime);
         System.out.println("Improved algorithm execution time (ns)...." + duration);
        
    }
    

    public static void naiveAlgorithm (java.util.List<String> wordList) {
        TreeMap wordTree = new TreeMap<String,Integer>();
        Integer frequency;
        
        for (String word : wordList){
            frequency = 0;
            for(String comparisonWord : wordList){
                if (word.equals(comparisonWord)){
                    frequency++;
                }
            }
            wordTree.put(word, frequency);
        }
    }
    
    public static void improvedAlgorithm (java.util.List<String> wordList){
        TreeMap<String,Integer> tm = new TreeMap<String,Integer>(); //What exactly makes this a 'tree'
        String key;
        Integer value;
        Iterator<String> it = wordList.iterator();
        
        while (it.hasNext()) {
            key = it.next();
            value = tm.get(key); 
            if (value == null){
                tm.put(key,1);
            }
            else {
                tm.put(key,value+1);
            }
        }
    }
}