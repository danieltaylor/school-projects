/*
CSIS 2420 Dr. Grady
Dan Taylor
Week 10, 6
8 Nov 2017

6. Write code that will give the words with the top 10 frequencies (this can be a naïve algorithm).
*/

import java.io.*;
import java.util.*;

public class Week10_6 {
    public static void main(String[] args) throws IOException {
        String[] wordArray = utilities.fileToStringArray("./awmt.txt");
        java.util.List<String> wordList = Arrays.asList(wordArray);
        TreeMap<String,Integer> tm = improvedAlgorithm(wordList);
        
        for (int i =0; i < 10; i++){
            int biggestKey = 0;
            Map.Entry<String,Integer> biggest = tm.firstEntry();
            
            for (Map.Entry<String,Integer> mapEntry : tm.entrySet()) {
                if (mapEntry.getValue() > biggestKey){
                    biggestKey = mapEntry.getValue();
                    biggest = mapEntry;
                    
                }
            }
            
            System.out.println("The word '" + biggest.getKey() + "' appeared " + biggest.getValue() + " times.");
            
            tm.remove(biggest.getKey());
        }
    }
    
    public static TreeMap improvedAlgorithm (java.util.List<String> wordList){
        TreeMap<String,Integer> tm = new TreeMap<String,Integer>(); //What exactly makes this a 'tree'
        String key;
        Integer value;
        Iterator<String> it = wordList.iterator();
        
        while (it.hasNext()) {
            key = it.next();
            value = tm.get(key); 
            if (value == null){
                tm.put(key,1);
            }
            else {
                tm.put(key,value+1);
            }
        }
        return tm;
    }
}