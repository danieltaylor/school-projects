import java.io.*;
import java.util.*;

//What is a set in algebra? The number of unique elements 
//Collection classes: you can write code in only a few lines that does a lot of work. 
//A set is a collection of things that are unique. 
//TreeSets will give unique values and sort them....



public class set03 { //Introduction to iterators now we will do filters
    public static void main (String[] args) throws IOException {
        String[] wordArray = utilities.fileToStringArray("./awmt.txt");
        java.util.List<String> wordList = Arrays.asList(wordArray);
        
        //TreeSet<String> ts = new TreeSet<String>(wordList);
        ArrayList<String> aL = new ArrayList<String>(wordList);
        
        System.out.println("aL starts with this many elements" + aL.size());
        
        Iterator<String> it = aL.iterator();
        
        while (it.hasNext()) {
            if (cond(it.next())){
                it.remove();
            }
        }
        
        for (String s : aL)
            System.out.println(s);
        
        System.out.println("aL now has this many elements " + aL.size());
        /*for (String s : aL) {
            System.out.println(s);
        }
        
        System.out.println("Our set has this many elements: " + ts.size());*/
    }
    
    public static boolean cond (String str) {
        return (str.length() > 5);
    }
}