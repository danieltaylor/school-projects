//CSIS 2420 Dr. Grady
//Dan Taylor
//Week 4, 6
//29 Sep 2017

//6. Describe in detail the method Java uses to hash strings.

//Hashing begins with a String. Java assigns each character of the string a 
//hashed value by multiplying the hash by 31 then subtracting the value of the 
//character. Then, java takes all the values together as the hash of the string. 