//CSIS 2420 Dr. Grady
//Dan Taylor
//Week 9, 4
//3 Nov 2017

//4. Demo the following ArrayList methods: contains, remove, clear and isEmpty using our data set (awmt.txt).

 
import java.io.*;
import java.util.*;
 
 public class Week9_4 {
     public static void main(String[] args){
        String rawtext = utilities.fileToString(args[0]);
        String cleanTxt = utilities.cleanText(rawtext);
        String[] wordArray = cleanTxt.split(" ");
        java.util.ArrayList<String> arrayList = new ArrayList(Arrays.asList(wordArray));
        
        
        //System.out.println(arrayList.peek());
        
        
        arrayList.add("001");
        //System.out.println(arrayList.peek());
        
        System.out.println("arrayList size: " + arrayList.size());
        
        System.out.println("True or false, contains 001 " + arrayList.contains("001"));
        
        arrayList.remove("001");
        System.out.println("True or false, contains 001 " + arrayList.contains("001"));
        
        arrayList.clear();
        System.out.println("arrayList size: " + arrayList.size());
        
        System.out.println("True or false, isEmpty " + arrayList.isEmpty());
        
        
        
    }
     
 }