//Dan Taylor
//Queues using inheritance
//11 Sep 2017

public class Queue extends List {
	public Queue () {
		super("My queue");	
	}
	
	public void enqueue (Object obj) {
		super.insertAtBack(obj);
	}
	
	public Object dequeue () {
		if (isEmpty()){
			return null;
		}
		return super.removeFromFront();
	}
	
	public boolean isEmpty(){
		return super.isEmpty();
	}
	
	
	//Not part of the ADT, remove when done
	public String print () {
		return super.print();
	}
}