//Writing our own way of comparing

public class OrderedPair implements Comparable<OrderedPair> {
    public String key;
    public Integer value;
    
    public OrderedPair (String word, Integer freq){
        key = word; 
        value = freq;
    }
    
    public String toString(){
        int numSpaces = 30 - key.length();
        String spaces = "";
        for (int i = 0; i < numSpaces; i++){
            spaces = spaces + ".";
        }
        String silly = "" + key + spaces + value + "\n";
        return silly;
    }
    
    public int compareTo (OrderedPair op){
        return (this.value - op.value);
    }
}