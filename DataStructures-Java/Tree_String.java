// Basic BST

public class Tree_String {
	
	public TreeNode_Strings root;
	//private TreeNode root;
	
	public Tree_String (String key) {
		root = new TreeNode_Strings(key);
	}
	
	public static void insertNode (TreeNode_Strings t, String d) {
		
		//using this treeNode as our comparison node. 
		
		if (d.compareTo(t.getKey()) >= 0) {
			if (t.getLeft() == null) {
				t.setLeft(new TreeNode_Strings(d));
			}
			else {
				insertNode(t.getLeft(), d);
			}
		}
		
		else if (d.compareTo(t.getKey()) < 0) {
			if (t.getRight() == null){
				t.setRight(new TreeNode_Strings(d));
			}
			else {
				insertNode(t.getRight(), d);
			}
		}
	}
	
	//Here is a smart way, using recursion, to traverse the entire tree. 
	public static void inOrderTraversal (TreeNode_Strings  node){
        if (node == null){
            return;
        }
        
        inOrderTraversal(node.getLeft());
        System.out.print(node.getKey() + " ");
        
        inOrderTraversal(node.getRight());
        System.out.print(node.getKey() + " ");
    }
}