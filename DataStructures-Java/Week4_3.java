//CSIS 2420 Dr. Grady
//Dan Taylor
//Week 4, 3
//29 Sep 2017

//3. Generate 16 random ints in the range 0 to 100.  Demonstrate by hand the 
//insertion of these numbers into a hash array of size 31.  Resolve the 
//collisions by chaining.

import java.util.Random;

public class Week4_3 {
    public static void main(String[] args){
        int[] ints = new int[16];
        for (int i = 0; i < 16; i++){
            ints[i]= (int) (Math.random()*100);
            System.out.println(ints[i]);
        }
    }
}