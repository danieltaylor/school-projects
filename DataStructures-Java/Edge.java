public class Edge {
    private Vertex label1;
    private Vertex label2;
    private int weight;
    
    public Edge(Vertex one, Vertex two, int weight) {
        this.label1 = one;
        this.label2 = two; 
        this.weight = weight;
    }
    
    public void print (){
        System.out.println("Edge between " + label1.getLabel() + " and " + label2.getLabel() +  ", weight " + weight);
    }

}