// Basic BST

public class Tree_String {
	
	
	public TreeNode root;
	//private TreeNode root;
	
	public Tree (String key) {
		root = new TreeNode(key);
	}
	
	public void insertNode (TreeNode_Strings t, String d) {
		
		//using this treeNode as our comparison node. 
		
		if (d.compareTo(t.getKey())) {
			if (t.getLeft() == null) {
				t.setLeft(new TreeNode(d));
			}
			else {
				insertNode(t.getLeft(), d);
			}
		}
		
		else if (d > t.getKey()) {
			if (t.getRight() == null){
				t.setRight(new TreeNode(d));
			}
			else {
				insertNode(t.getRight(), d);
			}
		}
	}
	
	//Here is a smart way, using recursion, to traverse the entire tree. 
	public void inOrderTraversal (TreeNode  node){
        if (node == null){
            return;
        }
        
        inOrderTraversal(node.getLeft());
        System.out.print(node.getKey() + " ");
        
        inOrderTraversal(node.getRight());
        System.out.print(node.getKey() + " ");
    }
}