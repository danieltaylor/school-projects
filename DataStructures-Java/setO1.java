import java.io.*;
import java.util.*;

//What is a set in algebra

public class set01 {
    public static void main (String[] args) throws IOException {
        String[] wordArray = utilities.fileToStringArray("./awmt.txt");
        List<String> wordList = Arrays.asList(wordArray);
        HashSet<String> hs = new HashSet<String>(wordList);
        
        for (String s : hs) {
            System.out.println(s);
        }
        
        System.out.println("Our set has this many elements: " + hs.size());
    }
}