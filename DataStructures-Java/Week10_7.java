/*
CSIS 2420 Dr. Grady
Dan Taylor
Week 10, 7
8 Nov 2017

7. When you print out the data from a HashSet, does it follow the same order you would expect from the order in the text? Why or why not?

    No, hashed values have little to do with order in the text and appear psuedorandom. This is because the hash of a string is a derived numerical value based on the values assigned to each character, which has nothing to do with normal alphanumeric ordering.
*/