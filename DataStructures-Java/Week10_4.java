/*
CSIS 2420 Dr. Grady
Dan Taylor
Week 10, 4
8 Nov 2017
*/

//4. Form the union, intersection and difference of sets A and B from the sets found in exercise 2. How many elements in each of these sets? Do the results make sense?


import java.io.*;
import java.util.*;


public class Week10_4 {
    public static void main(String[] args) throws IOException{
        
        //Create a List then a TreeSet out of awmt.txt
        String[] wordArray = utilities.fileToStringArray("./awmt.txt");
        java.util.List<String> wordList = Arrays.asList(wordArray);
        TreeSet<String> wordSet = new TreeSet<String>(wordList);
        
        //Create Iterator and walk through TreeSet, create setA (<7 char words)
        Iterator<String> ity = wordSet.iterator();
        TreeSet<String> setA = new TreeSet<String>();
        while (ity.hasNext()){
            String ele = ity.next();
            if(setACondition(ele)){ //adds to setA if str < 7
                setA.add(ele);
            }
        }
       
        //Create Iterator and walk through TreeSet, create setB (>3 char words)
        Iterator<String> it = wordSet.iterator();
        TreeSet<String> setB = new TreeSet<String>();
        while (it.hasNext()){
            String elem = it.next();
            if(setBCondition(elem)){ //adds to setA if str > 3
                setB.add(elem);
            }
        }
        
        //Use setA and setB to test implementation of (A Union B)
        TreeSet<String> unionAB = union(setA, setB);
        System.out.println("setA size = " + setA.size() + ", setB size = " + 
            setB.size() + ", union size.........." + unionAB.size());
    
        //Use setA and setB to test implementation of (A Intersection B)
        TreeSet<String> intersectionAB = intersection(setA, setB);
        System.out.println("setA size = " + setA.size() + ", setB size = " +
            setB.size() + ", intersection size..." + intersectionAB.size());
        
        //Use setA and setB to test implementation of (A Difference B)
        TreeSet<String> differenceAB = difference(setA, setB);
        System.out.println("setA size = " + setA.size() + ", setB size = " +
            setB.size() + ", difference size......" + differenceAB.size());
            
        System.out.println("These results make sense. " + intersectionAB.size() + " + " + differenceAB.size() + " =  " + unionAB.size()); 
        
    }
    
    public static boolean setACondition (String str){
        char firstLetter = str.charAt(0);
        if (firstLetter == 'a' || firstLetter == 'b' || firstLetter == 'c' || firstLetter == 'd'|| firstLetter == 'A' || firstLetter == 'B' || firstLetter == 'C' || firstLetter == 'D'){
            return true;
        }
        return false;
    }
    
    public static boolean setBCondition (String str) {
        char firstLetter = str.charAt(0);
        if (firstLetter == 'c' || firstLetter == 'd' || firstLetter == 'e' || firstLetter == 'f' || firstLetter == 'C' || firstLetter == 'D' || firstLetter == 'E' || firstLetter == 'F'){
            return true;
        }
        return false;
    }
    
    public static TreeSet<String> union (TreeSet<String> inputTreeSet, 
        TreeSet<String> inputTreeSet2) { 
        
        
        TreeSet<String> aCopy = new TreeSet<String>(inputTreeSet);
        TreeSet<String> bCopy = new TreeSet<String>(inputTreeSet2);
        TreeSet<String> union = aCopy;
        union.addAll(bCopy);
        
        return union;
    }
    
    public static TreeSet<String> intersection (TreeSet<String> inputTreeSet, 
        TreeSet<String> inputTreeSet2) { 
        
        TreeSet<String> aCopy = new TreeSet<String>(inputTreeSet);
        TreeSet<String> bCopy = new TreeSet<String>(inputTreeSet2);
        TreeSet<String> intersection = aCopy;
        intersection.retainAll(bCopy);
        
        return intersection;
    }
    
    public static TreeSet<String> difference (TreeSet<String> inputTreeSet, 
        TreeSet<String> inputTreeSet2) { 
    
        TreeSet<String> unionSet = union(inputTreeSet, inputTreeSet2);
        TreeSet<String> intersectionSet = intersection(inputTreeSet, inputTreeSet2);
        unionSet.removeAll(intersectionSet);
        TreeSet<String> difference = unionSet;
        
        return difference;
    }
}