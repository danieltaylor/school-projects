//CSIS 2420 Dr. Grady
//Dan Taylor
//Week 3, 4
//22 Sep 2017

//4. Write a method that prints out all the keys in a binary search tree (even 
//if it has 10 million nodes).  If you have trouble doing this, at least print 
//out 10 values in any way you can.
import java.util.concurrent.ThreadLocalRandom;

public class Week3_4 {
    public static void main(String[] args){
        
        int key = ThreadLocalRandom.current().nextInt(0,1000);
        
        Tree exerciseTree = new Tree(key);
        
        for (int i =0; i < 15; i++){
            exerciseTree.insertNode(exerciseTree.root, ThreadLocalRandom.current().nextInt(0,1000));
        }
        
        //See Tree.java
        exerciseTree.inOrderTraversal(exerciseTree.root);
    }
   
}