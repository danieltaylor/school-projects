//CSIS 2420 Dr. Grady
//Dan Taylor
//Hash Table, Collision resolution by chaining
//20 Sep 2017

public class myHashTable {
    public static void main(String[] args){
        List[] hashTable = new List[23];
        
        int key; 
        int value;
        
        //With object arrays, always have to initialize the array. 
        for (int i = 0; i < 23; i++){
            hashTable[i] = new List();
        }
        
        for (int i = 0; i < 15; i++){
            value = (int) (Math.random() * 1000);
            key = value % 23;
            hashTable[key].insertAtFront(value);
        }
        
        for (int i = 0; i < 23; i++){
            System.out.println("Index in the hashTable: "+ i + " " + hashTable[i].print());
        }
        System.out.println(Integer.MAX_VALUE);
        
    }
    
}


//Testing DAWG
