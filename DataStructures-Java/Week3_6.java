//CSIS 2420 Dr. Grady
//Dan Taylor
//Week 3, 6
//22 Sep 2017

//6. How many nodes does a balanced binary tree with 20 levels have?  How many 
//different shades of red can we express with 8 bits?  

//A balanced binary tree has 2^n nodes, where n is the number of levels. With 
//20 levels, the tree would contain 2^20 or 1,024,576 nodes.

//Because we express red with 8 bits, we can generate 2^8 shades of red, or 256
//shades of red. That is to say, 8 binary digits have a maximum value of 255 
//including 0 to make 256 shades of red possible.