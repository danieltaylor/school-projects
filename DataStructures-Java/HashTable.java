//CSIS 2420 Dr. Grady
//Dan Taylor
//Week 4, 4
//29 Sep 2017

//4. Convert the code we did in class into a working specialized data structure
//called “HashTable”.
import java.util.ArrayList;

public class HashTable {
    public List[] hashTable;
    private int maxSize; 
    private int capacity;
    
    public HashTable(int maxSize){
        if(maxSize > 0){
            this.maxSize = maxSize;
        }
        
        this.capacity = maxSize + (maxSize/2);
        this.hashTable = new List[capacity];
        for (int i = 0; i < this.capacity; i++){
            hashTable[i] = new List();
        }
    }
    
    public void addIntValue (int input){
        int position = input % capacity ;
        hashTable[position].insertAtFront(input);
    }
    
    public void addStringValue (String input){
        int position = input.hashCode() % this.capacity;
        if(position < 0){
            position = position * -1;
        }
        hashTable[position].insertAtFront(input);
    }
}