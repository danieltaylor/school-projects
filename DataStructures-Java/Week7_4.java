/* 4. Write a test driver to illustrate the following methods of Java’s LinkedList class:
addFirst
addLast
clear
get
getFirst
getLast
indexOf
peek
poll
pop
push
removeFirst
removeLast
size */
import java.util.LinkedList;

public class Week7_4 { 
    public static void main (String[] args) {
        LinkedList ll = new LinkedList();
        String silly = "silly";
        ll.addFirst(silly);
        ll.addLast(5);
        ll.clear();
        ll.addLast(7);
        System.out.println(ll.getFirst());
        System.out.println(ll.getLast());
        ll.addFirst(silly);
        ll.addLast(5);
        System.out.println(ll.peek());
        ll.push(1);
        ll.push(2);
        ll.push(3);
        ll.removeFirst();
        ll.removeLast();
        ll.pop();
        System.out.println(ll.size());
    }
}