//Dan Taylor
//Binary Tree Node
//For STrings

public class TreeNode_Strings {
	private String key; 
	private TreeNode_Strings leftNode; 
	private TreeNode_Strings rightNode;
	//private Record myRecord; 
	
	//In Binary Search Tree we sort on the keys, but there will be data 
	//associated with a given key. 
	//This could be very complicated 
	
	
	public TreeNode_Strings (String s) {
		key = s; 
		leftNode = rightNode = null;
	}
	
	
	public String getKey () {
		return key;
	}
	
	public TreeNode_Strings getLeft() {
		return leftNode;
	}
	
	public TreeNode_Strings getRight() {
		return rightNode;
	}

	public void setLeft (TreeNode_Strings left) {
		leftNode = left;
	}
	
	public void setRight (TreeNode_Strings right) {
		rightNode = right;
	}
	
	public void setKey (String s) {
		key = s;
	}
	
}
