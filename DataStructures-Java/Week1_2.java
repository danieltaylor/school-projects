import java.util.Arrays;
import java.util.Scanner;
import java.util.concurrent.ThreadLocalRandom;

//CSIS 2420 Dr. Grady
//Dan Taylor
//Week 1, 2
//Test your environment by writing a static method that takes positive integer n as input and outputs an array of n random integers 
//in the range 0 to 1,000,000.  Test this method by calling it on n = 100 and printing the result.

public class Week1_2 {

	public static void main(String[] args) {
		//Takes input from user
		Scanner gimmeDatInteger  = new Scanner(System.in);
		System.out.println("Gimme an INT");
		int n = gimmeDatInteger.nextInt();
		
		//Calls method to generate array
		int[] intArray = takesInIntOutputsArray(n);
		
		//Prints the results
		System.out.println("Your random int array of size " + n + ": " + Arrays.toString(intArray));
	}
	
	public static int[] takesInIntOutputsArray(int n) {
		int[] intArray = new int[n];
		
		//Populates array of size n with random integers between 0 and 1000000
		for (int i = 0; i < n; i ++) {
			intArray[i] = ThreadLocalRandom.current().nextInt(0,1000001);
		}
		return intArray;
	}
}