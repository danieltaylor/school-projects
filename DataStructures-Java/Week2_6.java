//CSIS 2420 Dr. Grady
//Dan Taylor
//Week 2, 6
//13 Sep 2017

//6. Write a test driver for the second version of the Stack class and verify all the methods work.

public class Week2_6 {
    
    //Test driver for Stack v2
    public static void main(String[] args){
        
        //Testing constructor
        Stack exerciseStack = new Stack();
        
        
        //Testing push
        Object exerObj = "ExerObj1";
        Object silly = "Silly";
        Object danger = "noodles";
        exerciseStack.push(exerObj);
        
        
        //Testing print
        System.out.println(exerciseStack.print());
        
        //Testing isEmpty, part 1
        System.out.println("The Stack is empty, true/false : " + exerciseStack.isEmpty());
        
        //Testing pop
        System.out.println("\nTesting pop, we just popped off : " + exerciseStack.pop());
        System.out.println("Now the list is..");
        System.out.println(exerciseStack.print());
        
        
        //Testing isempty part 2
        System.out.println("The Stack is empty, true/false : " + exerciseStack.isEmpty());
        
        //Testing print
        System.out.println("\nPushing two more things on the stack, and testing print");
        exerciseStack.push(silly);
        exerciseStack.push(danger);
        System.out.println(exerciseStack.print());
        
    }
}