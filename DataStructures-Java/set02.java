import java.io.*;
import java.util.*;

//What is a set in algebra
//Collection classes: you can write code in only a few lines that does a lot of work. 
//A set is a collection of things that are unique. 
//TreeSets will give unique values and sort them....

public class set02 {
    public static void main (String[] args) throws IOException {
        String[] wordArray = utilities.fileToStringArray("./awmt.txt");
        java.util.List<String> wordList = Arrays.asList(wordArray);
        
        TreeSet<String> ts = new TreeSet<String>(wordList);
        //ArrayList<String> aL = new ArrayList<String>(wordList);
        
        //System.out.println("aL starts with this many elements" + aL.size());
        
        for (String s : ts) {
            System.out.println(s);
        }
        
        System.out.println("Our set has this many elements: " + ts.size());
    }
}