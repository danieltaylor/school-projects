import java.io.*;
import java.util.*;

//What is a set in algebra? The number of unique elements 
//Collection classes: you can write code in only a few lines that does a lot of work. 
//A set is a collection of things that are unique. 
//TreeSets will give unique values and sort them....



public class set04 { //Introduction to iterators now we will do filters
    public static void main (String[] args) throws IOException {
        String[] wordArray = utilities.fileToStringArray("./awmt.txt");
        java.util.List<String> wordList = Arrays.asList(wordArray);
        TreeSet<String> setA = new TreeSet<String>(wordList);
        TreeSet<String> setB = new TreeSet<String>(wordList);
        TreeSet<String> setC;
        Iterator<String> it;
        
        it = setA.iterator();
        
        
        
        
        
        
        System.out.println("setA starts with this many elements" + setA.size());
        while (it.hasNext()) { //Removes all big words
            if (cond(it.next())){
                it.remove();
            }
        }
        System.out.println("setA now has this many elements after applying the filter" + setA.size());
        
        
        
        
        
        
        System.out.println("setB starts with this many elements" + setB.size());
        it = setB.iterator();
        
        while (it.hasNext()) { //Removes all words greater than 3
            if (cond2(it.next())){
                it.remove();
            }
        }
        System.out.println("setB now has this many elements after applying the filter" + setA.size());
        
        
        
        
        
        
        setC = intersection(setA, setB);
        System.out.println("setC, the intersection of the above, has this many elements: " + setA.size());
    }
    
    public static boolean cond (String str) {
        return (str.length() > 5);
    }
    
    public static boolean cond2 (String str) {
        return (str.length() < 3);
    }
    
    public static TreeSet<String> intersection (TreeSet<String> inputTreeSet, TreeSet<String> inputTreeSet2) { 
        TreeSet<String> Acopy = inputTreeSet;
        //TreeSet<String> BCopy = inputTreeSet2;
        Acopy.retainAll(inputTreeSet2);
        return Acopy;
    }
}