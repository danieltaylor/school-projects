//Array based heap

public class minHeap {
    private int heapArray[];
    private int maxSize;
    private int currentSize;
    
    public minHeap(int maximiumHeapSize) {
        maxSize = maximiumHeapSize;
        
        currentSize = 0;
        heapArray = new int[maxSize];
        
    }
    
    public boolean isEmpty() {
        return (currentSize==0);
        
    }
    
    public boolean insertItem(int key){
        if (currentSize < maxSize){
            heapArray[currentSize] = key;
            
            //Post increment, will increment the variable after the method call. 
            reHeapifyUp(currentSize++);
            return true;
            
        } else {
            return false;
        }
    }
    
    public void reHeapifyUp(int currentSize){
        //Parent index is subtract 1, divide by two.
        
        
        int parentIndex = (currentSize-1)/2; //This automatically gives us the floor. 
        int child = heapArray[currentSize];
        
        //while (child > heapArray[parentIndex]){ I had the right idea with this
        
        //Pushes the index of lesser numbers down. 
        while (currentSize > 0 && (heapArray[parentIndex] > child)){  
            heapArray[currentSize] = heapArray[parentIndex]; //There is no variable called heap
            currentSize = parentIndex;
            parentIndex = (parentIndex-1)/2;
        }
        
        heapArray[currentSize] = child;
    }
    
    //Should replace the top value with the last value, then reheapify
    public int remove() {
        if (isEmpty()){
            return -1;
        } else {
            int root = heapArray[0]; //Why are we storing this var? To return it?
            heapArray[0] = heapArray[--currentSize];
            reHeapifyDown(0);
            return root;
        }
    }
    
    //How is this different from reHeapifyUP?
    public void reHeapifyDown (int startHere){
        //int largerchild; 
        int top = heapArray[startHere];
        int leftChild; 
        int rightChild;
        int smallerChild;
        
        while (startHere < currentSize/2){
            leftChild = 2*startHere + 1;
            rightChild = 2*startHere +2;
            
            if(rightChild < currentSize && heapArray[leftChild] < heapArray[rightChild]){
                smallerChild = rightChild;
                
            }else {
                smallerChild = leftChild;
            } 
            
            if(top >= heapArray[smallerChild]){
                break;
            }
            
            heapArray[startHere] = heapArray[smallerChild];
            startHere = smallerChild;
        }
        heapArray[startHere] = top;
    }
}