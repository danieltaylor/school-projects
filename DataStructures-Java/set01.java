import java.io.*;
import java.util.*;

//What is a set in algebra
//Collection classes: you can write code in only a few lines that does a lot of work. 
//A set is a collection of things that are unique. 
//TreeSets will give unique values and sort them....

public class set01 {
    public static void main (String[] args) throws IOException {
        String[] wordArray = utilities.fileToStringArray("./awmt.txt");
        java.util.List<String> wordList = Arrays.asList(wordArray);
        HashSet<String> hs = new HashSet<String>(wordList);
        
        for (String s : hs) {
            System.out.println(s);
        }
        
        System.out.println("Our set has this many elements: " + hs.size());
    }
}