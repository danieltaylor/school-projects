//CSIS 2420 Dr. Grady
//Dan Taylor
//Week 1, 1
//For each of these pre-computer data structures, describe the problems they solve and any associated algorithms they use.



//Lists
//Lists solve the problem of having a lot of things to remember. A list may even be sorted to provide more meaning. 
//List algorithms include being alphabetized, chronologically sorted, sorted in order of importance, etc. 

//Charts
//Charts solve the problem of conveying spatial information to others. 
//Charts use spatial techniques like longitude and lattitude, plus color coding, to convey information. 

//File cabinet
//file cabinets solve the problem of having a lot of loose files. 
//file cabinets use an algorithm to cluster files together based on meaning; it could be chronologic, importance, alphabet, etc. 

//Dictionary
//They solve the problem of codifying every official word in a language and providing its meaning. 
//Uses alphabetizing to sort the data. 

//Maps
//solve the problem of conveying spatial information to others.
//Use spatial organization like lattitude and longitude, color coding, and map keys to organize data. 

//Rolodex
//Solves the problem of hvaing a lot of contact information
//Normally use an alphabetizing algorithm to sort the data

//Library
//Solves the problem of having many many books all at once
//Uses several different algorithms, for example the dewey decimal system. 

//Calendar
//solves the problem of organizing temporal data in a standard way
//Uses the 365 day, leap year, UTC convention algorithm to provide meaningful standard time

