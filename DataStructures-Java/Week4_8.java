//CSIS 2420 Dr. Grady
//Dan Taylor
//Week 4, 8
//29 Sep 2017

//8. Create an array of 15 names.  Use the Java hashCode function to insert 
//these into a (string) hash table of the appropriate size.  Then walk through 
//the table printing all values inserted at each index.
import java.util.Random;
public class Week4_8 {
    public static void main(String[] args){
        String[] names = new String[15];
        HashTable exerciseHashTable = new HashTable(15);
        for(String name : names){
            name = getSaltString();
            exerciseHashTable.addStringValue(name);
        }
        
        for(List list : exerciseHashTable.hashTable){
            System.out.print(list.print());
        }
    }
    
    protected static String getSaltString() {
        String SALTCHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        StringBuilder salt = new StringBuilder();
        Random rnd = new Random();
        while (salt.length() < 18) { // length of the random string.
            int index = (int) (rnd.nextFloat() * SALTCHARS.length());
            salt.append(SALTCHARS.charAt(index));
        }
        String saltStr = salt.toString();
        return saltStr;

    }
}