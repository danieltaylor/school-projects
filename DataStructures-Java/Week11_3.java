/*
CSIS 2420 Dr. Grady
Dan Taylor
10 Nov 2017
Week 11


3. Write a test driver for the ArrayDeque data structure.  Illustrate the 
following methods: add, addFirst, addLast, clear, contains, element, getFirst, 
getLast, peek, poll, pop, push and size.

*/

import java.util.*;

public class Week11_3 {
    public static void main(String[] args){
        ArrayDeque<String> ad = new ArrayDeque();
        System.out.println("\nUsing size() to get ArrayDeque's default size: " 
            + ad.size());
        
        System.out.println("Using add() to add 'Add' to ArrayDeque");
        ad.add("Add");
        
        System.out.println("Using addFirst() to add 'addFirst' to ArrayDeque");
        ad.addFirst("addFirst");
        
        System.out.println("Using contains() to see if ArrayDeque contains" 
            + " Add': " + ad.contains("Add"));
        
        System.out.println("Using getFirst() to get ArrayDeque's first element:"
            + " " + ad.getFirst());
        
        System.out.println("\nUsing clear() to remove everything from" 
            + " ArrayDeque");
        ad.clear();
        
        System.out.println("Using peek() to get the first element, should be"
            + " null: " + ad.peek());
            
        System.out.println("Using addLast() to add 'addLast' to ArrayDeque");
        ad.addLast("addLast");
        
        System.out.println("Using push() to push 'push' to ArrayDeque");
        ad.push("push");
        
        System.out.println("Using pop() to retrieve and remove the last element"
            + " of ArrayDeque: " + ad.pop());
        
        System.out.println("Using element() to retrieve but NOT remove head of" 
            + " ArrayDeque: " + ad.element()); 
            
        System.out.println("Using getLast() to retrieve but NOT remove the last" 
            + " element of ArrayDeque: " + ad.getLast());
            
        System.out.println("Using poll() to retrieve and remove the head of" 
            + " ArrayDeque: " + ad.poll());
            
        System.out.println("Using poll() to retrieve and remove the head of"
            + " ArrayDeque, should be null: " + ad.poll());
    }
}