import java.io.*;
//import java.util.*;

public class utilities {
    
    public static String[] fileToStringArray (String fileName) throws IOException {

        String data = fileToString(fileName);
        String str = cleanText(data);
        String[] wordArray = str.split(" ");
        
        return wordArray; 
    }
    
    public static String fileToString (String fileName) {
        String result = "";
        try{
            FileInputStream file = new FileInputStream(fileName);
            byte[] b = new byte[file.available()];
            file.read(b);
            file.close();
            result = new String(b);
            
        }catch (Exception e){
            System.out.println("Lol");
        }
        
        return result;
    }
    
    public static String cleanText (String silly){
        silly = silly.replaceAll("\\p{Punct}", "");
        silly = silly.replaceAll("\\s+", " ");
        return silly;
    }
    
}