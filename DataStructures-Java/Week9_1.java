//CSIS 2420 Dr. Grady
//Dan Taylor
//Week 9, 1
//3 Nov 2017

//1. Put our two new methods (fileToString and cleanText) in a utilities class.  Then rewrite FileToString03 to call those methods.
//Usage:java fileToString filename

import java.io.*;

public class Week9_1 { //Re-write of filetoString03
    public static void main (String[] args) throws IOException {
        String data = utilities.fileToString("./awmt.txt");
        String str = utilities.cleanText(data);
        String[] wordArray = str.split(" ");
        for (String word : wordArray){
            System.out.println(word);
        }
    }
}