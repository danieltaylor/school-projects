public class GraphTest {
    public static void main(String[] args) {
        Graph myGraph = new Graph();
        
        myGraph.addVertex("A");
        myGraph.addVertex("B");
        myGraph.addVertex("CSIS");
        myGraph.addVertex("Daniel");
        myGraph.addVertex("Edge");
        myGraph.addVertex("F");
        myGraph.addVertex("seven");
        myGraph.addVertex("eight");
        
        
        myGraph.addEdge(0,1);
        myGraph.addEdge(1,2);
        myGraph.addEdge(0,3);
        myGraph.addEdge(3,4);
        myGraph.addEdge(3,5);
        
        myGraph.displayVertices();
        myGraph.displayEdges();
    }
}