//CSIS 2420 Dr. Grady
//Dan Taylor
//Week 9, 2
//3 Nov 2017

//2. Using the SUU Public Meetings Policy pdf, clean up the text and parse it following the example we did in class.  Make sure to clean out all non-word symbols. (Hint: see the Pattern class). How many (real) words in this file? 

import java.util.regex.Pattern;
import java.util.regex.Matcher;
import java.io.*;
import java.util.*;


public class Week9_2 {
    public static void main(String[] args){
        String rawtext = utilities.fileToString(args[0]);
        String cleanTxt = utilities.cleanText(rawtext);
        String[] wordArray = cleanTxt.split(" ");
        java.util.ArrayList<String> arrayList = new ArrayList(Arrays.asList(wordArray));

        Pattern p = Pattern.compile("[a-zA-Z]*");
        
        
        for (String word : wordArray){
            Matcher m = p.matcher(word);
            boolean b = m.matches();
            if(b){
                
            }else{
                arrayList.remove(word);
            }
        }
        System.out.println("\n\nTotal of " + arrayList.size() + " (real) words.");
    }
    

}