//Count the freqeuncy of words in our file

import java.io.*;
import java.util.*;

//What is a set in algebra? The number of unique elements 
//Collection classes: you can write code in only a few lines that does a lot of work. 
//A set is a collection of things that are unique. 
//TreeSets will give unique values and sort them....

public class countwords { 
    public static void main (String[] args) throws IOException {
        String[] wordArray = utilities.fileToStringArray("./awmt.txt");
        java.util.List<String> wordList = Arrays.asList(wordArray);
        TreeMap<String,Integer> tm = new TreeMap<String,Integer>(); //What exactly makes this a 'tree'
        String key;
        Integer value;
        Iterator<String> it = wordList.iterator();
        
        while (it.hasNext()) {
            key = it.next();
            value = tm.get(key); 
            if (value == null){
                tm.put(key,1);
            }
            else {
                tm.put(key,value+1);
            }
        }
        
        Set<String> allTheKeys = tm.keySet();
        Iterator<String> printsTheKeys  = allTheKeys.iterator();
        
        while (printsTheKeys.hasNext()){
            key = printsTheKeys.next();
            value = tm.get(key);
            System.out.println(key + " -----> " + value);
        }
    }
}