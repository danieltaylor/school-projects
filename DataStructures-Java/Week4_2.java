//CSIS 2420 Dr. Grady
//Dan Taylor
//Week 4, 2
//29 Sep 2017

//2. Reimplement TreeNode, Tree and TreeNodeTest to work with Strings.  Use it
//to store the first 20 words in the “About SUU” web page.  Then print the 
//contents of the tree. (Hint: Use the String compareTo() method).

public class Week4_2 {
    public static void main(String[] args){
        String paragraph = "Southern Utah University is a caring campus community where students come to explore their interests and prepare for meaningful careers and life experiences. With more than 140 undergraduate and 19 graduate programs across six academic colleges, SUU proudly offers world-class, project-based learning opportunities where students gain professional experience before entering the job market.";
        
        //Store each of the first 20 words as a seperate String in array words[]
        String[] words = paragraph.split("\\s");
        String[] wordz = new String[20];
        for (int i = 0; i < 20; i++){
            wordz[i] = words[i].replaceAll("[^\\w]", "");
        }
        
        Tree_String exerciseTRee = new Tree_String(words[0]);
        
        for (String word : wordz){
            Tree_String.insertNode(exerciseTRee.root, word);
        }
    
        exerciseTRee.inOrderTraversal(exerciseTRee.root);
        
    }
}