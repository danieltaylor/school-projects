//CSIS 2420 Dr. Grady
//Dan Taylor
//Week 4, 1
//29 Sep 2017

//1. Add a find(key) method to the (integer) Tree class which returns true if 
//the key is in the Tree and false if key is not in the tree (Hint: most of the
//code you need is in insertNode()).

import java.util.concurrent.ThreadLocalRandom;


public class Week4_1 {
    public static void main(String[] args){
        int key1 = 10;
        Tree exerciseTree1 = new Tree(key1);
        
        for (int i =0; i < 15; i++){
            exerciseTree1.insertNode(exerciseTree1.root, 
            ThreadLocalRandom.current().nextInt(0,1000));
        }
        
        System.out.println(exerciseTree1.find(exerciseTree1.root, key1));
    }
}