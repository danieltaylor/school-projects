// Basic BST

public class Tree {
	
	
	public TreeNode root;
	//private TreeNode root;
	
	public Tree (int key) {
		root = new TreeNode(key);
	}
	private int maxi;
	private int min = Integer.MAX_VALUE;
	public void insertNode (TreeNode t, int d) {
		
		//using this treeNode as our comparison node. 
		
		if (d < t.getKey() ) {
			if (t.getLeft() == null) {
				t.setLeft(new TreeNode(d));
			}
			else {
				insertNode(t.getLeft(), d);
			}
		}
		
		else if (d > t.getKey()) {
			if (t.getRight() == null){
				t.setRight(new TreeNode(d));
			}
			else {
				insertNode(t.getRight(), d);
			}
		}
	}
	
	//Here is a smart way, using recursion, to traverse the entire tree. 
	public void inOrderTraversal (TreeNode node){
        if (node == null){
            return;
        }
        
        inOrderTraversal(node.getLeft());
        System.out.print(node.getKey() + " ");
        
        inOrderTraversal(node.getRight());
        System.out.print(node.getKey() + " ");
    }
    
    public void inOrderTraversalSilent (TreeNode node){
    	if (node == null){
            return;
        }
      
        inOrderTraversalSilent(node.getLeft());
        inOrderTraversalSilent(node.getRight());
    }
    
    public int max (TreeNode node){
    	if (node == null){
    		return maxi;
    	}
    	
    	max(node.getRight());
    	if (node.getKey() > maxi){
    		 maxi = node.getKey();
    	}
    	max(node.getLeft());
    	if (node.getKey() > maxi){
    		 maxi = node.getKey();
    	}
    	
    	return maxi;
    }
    
    public int min (TreeNode node){               
    	if (node == null){
    		return min;
    	}
    	
    	min(node.getRight());
    	if (node.getKey() < min){
    		min = node.getKey();
    	}
    	min(node.getLeft());
    	if (node.getKey() < min){
    		min = node.getKey();
    	}
    	
    	return min;
    }
    
    public boolean find(TreeNode t, int key){
    	boolean boo = false;
    	
    	if (key < t.getKey()){
    		if (t.getLeft() != null){
    			find(t.getLeft(), key);
    		}
    	} else if (key > t.getKey()){
    		if (t.getRight() != null){
    			find(t.getRight(), key);
    		}
    	} else if (key == t.getKey()){
    		boo = true;
    	}
    	return boo;
    }
}