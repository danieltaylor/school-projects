

public class TestDrive_ProjectHeap {
    public static void main(String[] args) {
        System.out.println("\nCreating a new heap of " + args[0] + 
            " randomly generated numbers");
            
        int newHeapSize = Integer.parseInt(args[0]); //Cast the input String to integer
        ProjectHeap newHeap = new ProjectHeap(newHeapSize);
        
        while (newHeapSize > 0){ //While newHeap is not empty... Fill it!
            newHeap.insertItem(5);
            newHeapSize--;
        }
        
        for (int number : newHeap.getHeap()){
            System.out.print(number);
        }
        
    }
}