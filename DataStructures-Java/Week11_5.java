/*
CSIS 2420 Dr. Grady
Dan Taylor
10 Nov 2017
Week 11


5. Write code that reverses the above: given a number written in binary 
notation, write a program that converts it to decimal.  It should be able to
handle numbers <= 2^64 -1. (64 bit numbers)

*/

import java.io.*;
import java.util.*;
import java.math.*;

public class Week11_5 {
    public static void main(String[] args){
        Scanner in = new Scanner(System.in);
        Deque<Integer> binStack = new ArrayDeque();
        BigInteger decimalValue = new BigInteger("0");
        String binaryInput = "";
        //while (true) {
            System.out.println("Give the comptuer a binary number up to 64"
                + " bits");
            //binaryInput = in.next();
            for (int i = 0; i < 64; i++){
                binaryInput += "1";
            }
            
            decimalValue = new BigInteger("0");
            System.out.println("Binary input: " + binaryInput);
            for (int i = 0; i < binaryInput.length(); i++){
                if(binaryInput.charAt(i) == '1'){
                    int sillyInt = binaryInput.length() - (i+1);
                    BigInteger silly = new BigInteger("2");
                    decimalValue = decimalValue.add(silly.pow(sillyInt));
                }
            }
            System.out.println("Decimal value: " + decimalValue);
            
        //}
    }
}