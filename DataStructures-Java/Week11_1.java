/*
CSIS 2420 Dr. Grady
Dan Taylor
10 Nov 2017
Week 11


1. Make the following improvements to the countwords02 program we did in class:
-- make it case insensitive
-- have it sort so the highest values are at the top.
-- clean up the output so it looks more professional.

*/

//Count the freqeuncy of words in our file

import java.io.*;
import java.util.*;

public class Week11_1_Countwords02 { 
    public static void main (String[] args) throws IOException {
        String[] wordArray = utilities.fileToStringArray("./awmt.txt");
        java.util.List<String> wordList = Arrays.asList(wordArray);
        TreeMap<String,Integer> tm = new TreeMap<String,Integer>(); //What exactly makes this a 'tree'
        String key;
        Integer value;
        Iterator<String> it = wordList.iterator();
        
        while (it.hasNext()) {
            key = it.next();
            value = tm.get(key); 
            if (value == null){
                tm.put(key,1);
            }
            else {
                tm.put(key,value+1);
            }
        }
        
        Set<String> allTheKeys = tm.keySet();
        Iterator<String> printsTheKeys  = allTheKeys.iterator();
        
        OrderedPair[] opArray = new OrderedPair[allTheKeys.size()];
        int index = 0;
        
        while (printsTheKeys.hasNext()){
            key = printsTheKeys.next();
            value = tm.get(key);
            System.out.println(key + " -----> " + value);
            opArray[index++]  = new OrderedPair(key,value);        
        }
        
        java.util.List<OrderedPair> opList = Arrays.asList(opArray);
        Collections.sort(opList);
        System.out.println(opList);
        
    }
}