/*
CSIS 2420 Dr. Grady
Dan Taylor
10 Nov 2017
Week 11


4. Improve the binaryConversion program to handle long integers.

*/

import java.io.*;
import java.util.*;

public class Week11_4_binaryConversion{
    public static void main(String[]args) throws IOException {
        Deque<Integer> stack = new ArrayDeque();
        Scanner in = new Scanner(System.in);
        long input;
        int rem;
        
        while(true){
            System.out.println("Enter a positive integer to conver to binary");
            input = in.nextLong();
            System.out.println("Tanks you entered a " );
            
            
           while (input != 0){
                rem = input % 2;
                stack.push(rem);
                input = input /2;
            }
            
            System.out.println("The input in binary number is " );
            
            while (!stack.isEmpty()){
                System.out.print(stack.pop());
            }
            
            System.out.println();
        }
    }
}