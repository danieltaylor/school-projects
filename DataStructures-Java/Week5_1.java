//CSIS 2420 Dr. Grady
//Dan Taylor
//Week 5, 1
//6 Oct 2017

//1. Generate 10 random ints in the range 0 to 1000. Create a heap (by hand) with these values.  Show 10 steps of the process. 

import java.util.Random;

public class Week5_1 {
    public static void main(String[] args){
        int[] ints = new int[10];
        for (int i = 0; i < 10; i++){
            ints[i]= (int) (Math.random()*1000);
            System.out.println(ints[i]);
        }
    }
}