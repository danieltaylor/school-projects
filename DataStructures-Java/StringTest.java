//Dan Taylor
//18 Sep 2017 

public class StringTest {
    public static void main(String[] args) {
        String s1 = "Zebra";
        String s2 = "Antelope"; 
        
        System.out.println("Comparing Zerba to Antelope: " + s1.compareTo(s2));
        System.out.println("Comparing Antelope to Zebra: " + s2.compareTo(s1));
        
        System.out.println("Comparing Antelope to Antelope: " + s2.compareTo(s2));
        
        
    }

}