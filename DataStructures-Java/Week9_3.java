//CSIS 2420 Dr. Grady
//Dan Taylor
//Week 9, 3
//3 Nov 2017

 //3. Demo the PriorityQueue methods: add, clear, contains, peak, remove, size using our data set.
 
import java.io.*;
import java.util.*;
 
 public class Week9_3 {
     public static void main(String[] args){
        String rawtext = utilities.fileToString(args[0]);
        String cleanTxt = utilities.cleanText(rawtext);
        String[] wordArray = cleanTxt.split(" ");
        java.util.ArrayList<String> arrayList = new ArrayList(Arrays.asList(wordArray));
        PriorityQueue exerQueue = new PriorityQueue(arrayList);
        
        
        System.out.println(exerQueue.peek());
        
        
        exerQueue.add("001");
        System.out.println(exerQueue.peek());
        
        System.out.println("ExerQueue size: " + exerQueue.size());
        
        System.out.println("True or false, contains 001 " + exerQueue.contains("001"));
        
        exerQueue.remove("001");
        System.out.println("True or false, contains 001 " + exerQueue.contains("001"));
        
        exerQueue.clear();
        System.out.println("ExerQueue size: " + exerQueue.size());

        
        
    }
     
 }
     