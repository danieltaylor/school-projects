/*
CSIS 2420 Dr. Grady
Dan Taylor
10 Nov 2017
Week 11


2. Create filters that select the following:
-- All words that are used more than 100 times.
--All words that are used more than 50 times.
--All words used more than 10 times.
Print out the sets and give their size for each category.

*/

import java.io.*;
import java.util.*;

public class Week11_2 {
    public static void main(String[] args) throws IOException {
        String[] wordArray = utilities.fileToStringArray("./awmt.txt");
        java.util.List wordList = Arrays.asList(wordArray);
        TreeMap<String,Integer> tm = new TreeMap<String,Integer>();
        String key; 
        Integer value;
        Iterator<String> it = wordList.iterator();
        
        while (it.hasNext()) {
            key = it.next();
            key = key.toLowerCase();
            value = tm.get(key); 
            if (value == null){
                tm.put(key,1);
            }
            else {
                tm.put(key,value+1);
            }
        }
        
        Set<String> allTheKeys = tm.keySet();
        Iterator<String> printsTheKeys  = allTheKeys.iterator();
        
        OrderedPair[] opArray = new OrderedPair[allTheKeys.size()];
        int index = 0;
        
        while (printsTheKeys.hasNext()){
            key = printsTheKeys.next();
            value = tm.get(key);
            opArray[index++]  = new OrderedPair(key,value);        
        }
        
        java.util.List<OrderedPair> opList = Arrays.asList(opArray);
        Collections.sort(opList, Collections.reverseOrder());
        
        filterWords(opList, 100);
        filterWords(opList, 50);
        filterWords(opList, 10);
    }
    
    public static void filterWords (java.util.List<OrderedPair> opList, int freq){
        int sizeOfSet = 0;
        for (OrderedPair op : opList){
            if (op.value > freq){
                System.out.print(op);
                sizeOfSet++;
            }
        }
        System.out.println("There are " + sizeOfSet + 
            " words with frequency > " + freq + "\n");
    }
}