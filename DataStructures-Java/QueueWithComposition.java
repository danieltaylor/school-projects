//CSIS 2420 Dr. Grady
//Dan Taylor
//Week 3, 1
//18 Sep 2017

//2. Write a Queue class using composition, and write an appropriate test 
//driver.  First, test it with the “print” method. Write a “peek()” method that 
//only returns the object at the front of the queue without removing it.

public class QueueWithComposition {
    private List queueList;
    private String name;
    
    public QueueWithComposition() {
        name = "QueueWithComposition";
        queueList = new List("QueueWithComposition");
    }
    
    public QueueWithComposition(String name) {
        this.name = name;
        queueList = new List(name);
    }
    
    public void enqueue (Object obj) {
        queueList.insertAtBack(obj);
    }
    
    public Object dequeue () {
        if (queueList.isEmpty()){
            return null;
        }
        
        return queueList.removeFromFront();
        
    }
    
    public boolean isEmpty(){
        return queueList.isEmpty();
    }
    
    public Object peek() {
        return queueList.firstNode.getObject();
    }
    
    //Not part of the ADT, remove when done?
    public String print() {
        return queueList.print();
        
    }
}