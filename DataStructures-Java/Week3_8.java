//CSIS 2420 Dr. Grady
//Dan Taylor
//Week 3, 8
//22 Sep 2017

//8. The IRS needs a balanced binary search tree with 450 million records.  How 
//many levels are required? 

//For a balanced tree, the number of nodes are 2^n. Where 450 million nodes are 
//required, there are log base 2 of 450 million, or 28.7 levels. Therefore, 
//29 levels are required.