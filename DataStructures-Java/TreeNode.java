//Dan Taylor
//Binary Tree Node

public class TreeNode {
	private int key; 
	private TreeNode leftNode; 
	private TreeNode rightNode;
	//private Record myRecord; 
	
	//In Binary Search Tree we sort on the keys, but there will be data associated with a given key. 
	//This could be very complicated 
	
	
	public TreeNode (int n) {
		key = n; 
		leftNode = rightNode = null;
	}
	
	
	public int getKey () {
		return key;
	}
	
	public TreeNode getLeft() {
		return leftNode;
	}
	
	public TreeNode getRight() {
		return rightNode;
	}

	public void setLeft (TreeNode left) {
		leftNode = left;
	}
	
	public void setRight (TreeNode right) {
		rightNode = right;
	}
	
	public void setKey (int n) {
		key = n;
	}
	
}
