//CSIS 2420 Dr. Grady
//Dan Taylor
//Week 3, 5
//22 Sep 2017

//5. Generate 1000 random ints in the range 0 to 1 million, print them in the 
//order they are generated and insert them into the tree.  Then do an 
//inOrderTraversal which prints out the keys.  What can you say about the values
//printed out?  How fast is this algorithm on 1 million keys where the keys 
//range from 0 to 1 billion (for timing purposes, you don’t need to print out). 
//(Hint: use System.currentTimeMillis()).

import java.util.concurrent.ThreadLocalRandom;

public class Week3_5 {
    public static void main(String[] args){
        
        
        int key1 = ThreadLocalRandom.current().nextInt(0,1000);
        
        Tree exerciseTree1 = new Tree(key1);
        
        for (int i =0; i < 15; i++){
            exerciseTree1.insertNode(exerciseTree1.root, ThreadLocalRandom.current().nextInt(0,1000));
        }
        
        //See Tree.java
        exerciseTree1.inOrderTraversal(exerciseTree1.root);
        
        int key = ThreadLocalRandom.current().nextInt(0,1000000000);
        Tree exerciseTree = new Tree(key);
        int var;
        for (int i = 0; i < 1000000; i++){
            var = ThreadLocalRandom.current().nextInt(0,1000000000);
            exerciseTree.insertNode(exerciseTree.root, var);
        }
        System.out.println("Time before executing tree traversal: " 
         + System.currentTimeMillis());
        exerciseTree.inOrderTraversalSilent(exerciseTree.root);
        System.out.println("Time after executing tree traversal:  " 
         + System.currentTimeMillis());
    }
}