//CSIS 2420 Dr. Grady
//Dan Taylor
//Week 2, 2
//13 Sep 2017

import java.math.*;


//2. Add the following method to the List code developed in class: removeFromBack(), which
//removes an object from the back of the list. Run a test driver for this new method using a List with
//the four elements above.
public class Week2_2 {
    
    //This is a test driver, for the method 'removeFromBack' in List.java
	public static void main(String[] args){
        List exerciseList = new List("Week2_2 Test List");
		
	//Four elements we used in class?
    	Character ch = new Character('$');
    	BigInteger bi1 = new BigInteger("111111111111111111111111111111");
    	BigInteger bi2 = new BigInteger("222222222222222222222222222222");
	    BigInteger bi3 = new BigInteger("333333333333333333333333333333");
	    

	    //Add elements one at a time and print what's in the list. 
	    exerciseList.insertAtBack(ch);
	    exerciseList.insertAtBack(bi1);
    	exerciseList.insertAtBack(bi2);
    	exerciseList.insertAtBack(bi3);
    	System.out.println("Here's a list...");
    	System.out.println(exerciseList.print());
    	
    	
    	System.out.println("\n\nNow we'll remove things from the back.\n");

    	exerciseList.removeFromBack();
    	System.out.println("One off the back");
    	System.out.println(exerciseList.print());

    	exerciseList.removeFromBack();
    	System.out.println("Two off the back");
    	System.out.println(exerciseList.print());

    	exerciseList.removeFromBack();
    	System.out.println("Three off the back");
    	System.out.println(exerciseList.print());
    	
        exerciseList.removeFromBack();
        System.out.println("Four off the back!");
    	System.out.println(exerciseList.print());
    	
    	System.out.println("Five off the back!");
        System.out.println(exerciseList.removeFromBack());
    }
}