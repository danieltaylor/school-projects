//CSIS 2420 Dr. Grady
//Dan Taylor
//Week 2, 1

import java.math.*;


//Add the following method to the List code developed in class: insertAtBack(). It should be able
//to add an object to the end of the list. Run a test driver for this new method using a List with the
//four elements we used in class. Show both the List code and the test driver code on your
//web page. Follow this pattern for all subsequent problems where you are asked to add
//new methods.
public class Week2_1 {
	public static void main(String[] args){
		List testList = new List("Week2_1 Test List");
		
		//Four elements we used in class?
		Character ch = new Character('$');
		BigInteger bi1 = new BigInteger("111111111111111111111111111111");
		BigInteger bi2 = new BigInteger("222222222222222222222222222222");
		BigInteger bi3 = new BigInteger("333333333333333333333333333333");
		
		System.out.println(testList.print());
		
		//Add element one, and print what's in the list. 
		testList.insertAtBack(ch);
		System.out.println(testList.print());
		
		//Add element two, and print what's in the list. 
		testList.insertAtBack(bi1);
		System.out.println(testList.print());
		
		//Add element three, and print what's in the list. 
		testList.insertAtBack(bi2);
		System.out.println(testList.print());
		
		//Add element four, and print what's in the list. 
		testList.insertAtBack(bi3);
		System.out.println(testList.print());
	}
}