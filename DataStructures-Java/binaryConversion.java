//Using arraydeque as a stack
//Pronounced array deck

import java.io.*;
import java.util.*;

public class binaryConversion{
    public static void main(String[]args) throws IOException {
        Deque<Integer> stack = new ArrayDeque();
        Scanner in = new Scanner(System.in);
        int input;
        int rem;
        
        while(true){
            System.out.println("Enter a positive integer to conver to binary");
            input = in.nextInt();
            System.out.println("Tanks you entered a " + input);
            
            
            while (input != 0){
                rem = input % 2;
                stack.push(rem);
                input = input /2;
            }
            
            System.out.println("The input in binary number is ");
            
            while (!stack.isEmpty()){
                System.out.print(stack.pop());
            }
            
            System.out.println();
        }
    }
}

//I want you guys to play with the ArrayDeque class. 