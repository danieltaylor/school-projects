//CSIS 2420 Dr. Grady
//Dan Taylor
//Week 6, 6
//13 Oct 2017


/* 6. Improve the Graph class so it uses the public getters and setters of the Vertex class. It should also be able to handle weighted graphs. Use your new Graph class to represent the weighted graph used in class: NetworkG.*/

public class Week6_6 {
    public static void main(String[] args){
        Graph myGraph = new Graph();
        
        myGraph.addVertex("A");
        myGraph.addVertex("B");
        myGraph.addVertex("C");
        myGraph.addVertex("D");
        myGraph.addVertex("E");
        myGraph.addVertex("F");
        myGraph.addVertex("G");
        myGraph.addVertex("H");
        
        
        myGraph.addEdge(0,1,12);
        myGraph.addEdge(0,2,3);
        myGraph.addEdge(0,4,6);
        myGraph.addEdge(2,5,18);
        myGraph.addEdge(1,3,5);
        myGraph.addEdge(1,4,5);
        myGraph.addEdge(4,6,22);
        myGraph.addEdge(3,5,4);
        myGraph.addEdge(3,7,19);
        myGraph.addEdge(5,7,10);
        myGraph.addEdge(6,7,2);
        
        myGraph.displayVertices();
        myGraph.displayEdges();
    }
}