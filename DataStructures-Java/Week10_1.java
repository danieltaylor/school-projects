//CSIS 2420 Dr. Grady
//Dan Taylor
//Week 10, 1
//8 Nov 2017

// 1. Write a filter that selects the words starting with ‘a’ or ‘b’ (it should not be case sensitive).  How big is the set of these words using the awmt.txt file?  (Hint: use compareTo()).

import java.io.*;
import java.util.*;


public class Week10_1 {
    public static void main(String[] args) throws IOException {
        String[] wordArray = utilities.fileToStringArray("./awmt.txt");
        java.util.List<String> wordList = Arrays.asList(wordArray);
        TreeSet<String> setA = new TreeSet<String>(wordList);
        TreeSet<String> setB = new TreeSet<String>(wordList);
        
        Iterator<String> it;
        it = setA.iterator();
        
        int allWordSetSize = setA.size();
        while(it.hasNext()){
            if(cond(it.next())){
                it.remove();
            }
        }
        int aOrBWordSetSize = setA.size();
        
        System.out.println("Number of words in awmt.txt: " + allWordSetSize + "\nNumber of words starting with a or b, case insensitive: " + aOrBWordSetSize);
        
    }
    
    public static boolean cond(String str){
        char firstLetter = str.charAt(0);
        if (firstLetter == 'a' || firstLetter == 'b' || firstLetter == 'A' || firstLetter == 'B'){
            return false;
        }
        return true;
    }
}