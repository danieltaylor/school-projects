//CSIS 2420 Dr. Grady
//Dan Taylor
//Week 4, 5
//29 Sep 2017

//5.  Using the hash table code given in class as a starting point, generate 100
//random ints in the range 0 to 1 million and insert these into an appropriately
//sized hash table.  Then walk through the table printing all values inserted 
//at each index.

import java.util.Hashtable;

public class Week4_5 {
    public static void main(String[] args){
        Hashtable exerciseHashtable = new Hashtable(100);
        int key;
        int value;
        for (int i = 0; i < 15; i++){
            value = (int) (Math.random() * 1000000);
            key = value % 150;
            exerciseHashtable.put(key, value);
        }
        
        
            System.out.println(exerciseHashtable.toString());
        
        
    }
}