/*
CSIS 2420 Dr. Grady
Dan Taylor
Term Project
20 Nov 2017

14. Implement and illustrate the Heap data structure using arrays from the 
ground up, in a way similar to how we implemented the List and Binary Search 
Tree data structures from the ground up.
*/

//Array based heap

public class projectHeap {
    private int heapArray[];
    private int maximumHeapSize;
    private int currentHeapSize;
    
    public Heap(int maximiumHeapSize) {
        this.maximumHeapSize = maximiumHeapSize;
        
        currentHeapSize = 0;
        heapArray = new int[maxSize];
        
    }
    
    public boolean isEmpty() {
        return (currentHeapSize == 0);
        
    }
    
    public boolean insertItem(int key){
        if (currentHeapSize < maxSize){
            heapArray[currentHeapSize] = key;
            
            //Post increment, will increment the variable after the method call. 
            reHeapifyUp(currentHeapSize++);
            return true;
            
        } else {
            return false;
        }
    }
    
    public void reHeapifyUp(int currentHeapSize){
        //Parent index is subtract 1, divide by two.
        
        
        int parentIndex = (currentHeapSize-1)/2; //This automatically gives us the floor. 
        int child = heapArray[currentHeapSize];
        
        //while (child > heapArray[parentIndex]){ I had the right idea with this
        
        //Pushes the index of lesser numbers down. 
        while (currentHeapSize > 0 && (heapArray[parentIndex] < child)){  
            heapArray[currentHeapSize] = heapArray[parentIndex]; //There is no variable called heap
            currentHeapSize = parentIndex;
            parentIndex = (parentIndex-1)/2;
        }
        
        heapArray[currentHeapSize] = child;
    }
    
    //Should replace the top value with the last value, then reheapify
    public int remove() {
        if (isEmpty()){
            return -1;
        } else {
            int root = heapArray[0]; //Why are we storing this var? To return it?
            heapArray[0] = heapArray[--currentHeapSize];
            reHeapifyDown(0);
            return root;
        }
    }
    
    //How is this different from reHeapifyUP?
    public void reHeapifyDown (int startHere){
        int largerchild; 
        int top = heapArray[startHere];
        int leftChild; 
        int rightChild;
        int largerChild;
        
        while (startHere < currentHeapSize/2){
            leftChild = 2*startHere + 1;
            rightChild = 2*startHere +2;
            
            if(rightChild < currentHeapSize && heapArray[leftChild] < heapArray[rightChild]){
                largerChild = rightChild;
                
            }else {
                largerChild = leftChild;
            } 
            
            if(top >= heapArray[largerChild]){
                break;
            }
            
            heapArray[startHere] = heapArray[largerChild];
            startHere = largerChild;
        }
        heapArray[startHere] = top;
    }
    
    public int[] getHeap () {
        return heapArray;
    }
    
    public void heapSort(int[] intArrayToInsert){
        Stack stackToReturn = new Stack();
        
        for(int i =0; i < maxSize; i++){
            reHeapifyUp(currentHeapSize-1);
            Object tempObj = remove();
            stackToReturn.push(tempObj);
        }
        
        System.out.println(stackToReturn.print());
    }
}