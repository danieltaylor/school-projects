//CSIS 2420 Dr. Grady
//Dan Taylor
//Week 2, 7
//13 Sep 2017

//7. Write code that pushes 10 strings onto the stack (version 2 of the stack), printing while it goes,
//then pops them off, printing while it removes.

public class Week2_7 {
    
    public static void main(String[] args){
        Stack exerciseStack = new Stack();
        
        for (int i = 1; i < 11; i++){
            //System.out.println(exerciseStack.print());
            exerciseStack.push(i);
            System.out.println(exerciseStack.print());
        }
        
        for (int i = 10; i > 0; i--){
            exerciseStack.pop();
            System.out.println(exerciseStack.print());
            
        }
    }    

}