/*

* @author  Daniel Taylor
* @version 1.0
* @since   13 November 2017

2. Write a filter that allows you to find the set A of words starting with a, b, c or d.  How many elements in the set? Write another filter that allows you to find the set B of words starting with c, d, e or f.  How many elements in the set?

*/




import java.io.*;
import java.util.*;


public class Week10_2 { //Introduction to iterators now we will do filters
    public static void main (String[] args) throws IOException {
        String[] wordArray = utilities.fileToStringArray("./awmt.txt");
        java.util.List<String> wordList = Arrays.asList(wordArray);
        TreeSet<String> setA = new TreeSet<String>(wordList);
        TreeSet<String> setB = new TreeSet<String>(wordList);
        TreeSet<String> setC;
        Iterator<String> it;
        
        it = setA.iterator();
        
        
        System.out.println("setA starts with this many elements........................." + setA.size());
        while (it.hasNext()) { //Removes words starting with notA, or notB
            if (cond(it.next())){
                it.remove();
            }
        }
        System.out.println("setA now has this many elements after applying the filter...." + setA.size());
        
        
        
        
        
        
        System.out.println("setB starts with this many elements........................." + setB.size());
        it = setB.iterator();
        
        while (it.hasNext()) { //Removes all words greater than 3
            if (cond2(it.next())){
                it.remove();
            }
        }
        System.out.println("setB now has this many elements after applying the filter...." + setB.size());
        
        
        
        
        
        
        setC = intersection(setA, setB);
        System.out.println("setC, the intersection of A and B, has this many elements...." + setA.size());
    }
    
    public static boolean cond(String str){
        char firstLetter = str.charAt(0);
        if (firstLetter == 'a' || firstLetter == 'b' || firstLetter == 'c' || firstLetter == 'd'|| firstLetter == 'A' || firstLetter == 'B' || firstLetter == 'C' || firstLetter == 'D'){
            return false;
        }
        return true;
    }
    
    public static boolean cond2 (String str) {
        char firstLetter = str.charAt(0);
        if (firstLetter == 'c' || firstLetter == 'd' || firstLetter == 'e' || firstLetter == 'f' || firstLetter == 'C' || firstLetter == 'D' || firstLetter == 'E' || firstLetter == 'F'){
            return false;
        }
        return true;
    }
    
    public static TreeSet<String> intersection (TreeSet<String> inputTreeSet, TreeSet<String> inputTreeSet2) { 
        TreeSet<String> Acopy = inputTreeSet;
        //TreeSet<String> BCopy = inputTreeSet2;
        Acopy.retainAll(inputTreeSet2);
        return Acopy;
    }
}