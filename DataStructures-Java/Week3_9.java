//CSIS 2420 Dr. Grady
//Dan Taylor
//Week 3, 9
//22 Sep 2017

//9. Add and test these methods to the (integer) Tree class:
//A. max() which returns the maximum value in the Tree.
//B. min() which returns the minimum value in the Tree.

import java.util.concurrent.ThreadLocalRandom;

public class Week3_9 {
    public static void main(String[] args){
        int key = ThreadLocalRandom.current().nextInt(0,1000);
        
        Tree exerciseTree = new Tree(key);
        
        for (int i =0; i < 15; i++){
            exerciseTree.insertNode(exerciseTree.root, ThreadLocalRandom.current().nextInt(0,1000));
        }
        exerciseTree.inOrderTraversal(exerciseTree.root); 
        
        System.out.println("\nMax integer val: " + exerciseTree.max(exerciseTree.root));
        System.out.println("Min integer val: " + exerciseTree.min(exerciseTree.root));
    }
}