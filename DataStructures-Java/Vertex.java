public class Vertex {
    private String label;
    public boolean wasVisited;
    //private Record record;
    
    
    public Vertex (String essay) {
        this.label = essay;
        wasVisited = false;
    }
    
    public String getLabel(){
          return label; 
    }
    
    public boolean hasBeenVisited(){
        return wasVisited;
    }
}