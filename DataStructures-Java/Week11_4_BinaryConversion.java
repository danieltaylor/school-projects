/*
CSIS 2420 Dr. Grady
Dan Taylor
10 Nov 2017
Week 11


4. Improve the binaryConversion program to handle long integers.

*/

import java.io.*;
import java.util.*;
import java.math.*;

public class Week11_4_BinaryConversion{
    public static void main(String[]args) throws IOException {
        Deque<Integer> stack = new ArrayDeque();
        Scanner in = new Scanner(System.in);
        String input;
        BigInteger rem;
        
        while(true){
            System.out.println("Enter a positive integer to conver to binary");
            input = in.next();
            BigInteger biggy = new BigInteger(input);
            BigInteger two = new BigInteger("2");
            
            
            while (biggy.intValue() != 0){
                rem = biggy.remainder(two);
                stack.push(rem.intValue());
                biggy = biggy.divide(two);
            }
            
            System.out.println(input + " in binary is: ");
            
            while (!stack.isEmpty()){
                System.out.print(stack.pop());
            }
            
            System.out.println();
        }
    }
}