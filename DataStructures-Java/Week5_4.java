//CSIS 2420 Dr. Grady
//Dan Taylor
//Week 5, 4
//6 Oct 2017

//4.  Starting with the Heap and HeapTest code given in class, write a method called “HeapSort” (as part of the Heap class) that takes an integer array as input and outputs the values in ascending order.  You can either use the min priority heap from ex. 3 or add a stack to the original code to reverse the order.

public class Week5_4 {
    public static void main(String[] args){
        Heap exerciseHeap = new Heap(10);
        
        for (int i = 0; i < 10; i++){
            exerciseHeap.insertItem(i);
        }
        
        int[] heap = exerciseHeap.getHeap();
        
        exerciseHeap.heapSort(heap);
    }
}