//CSIS 2420 Dr. Grady
//Dan Taylor
//Week 2, 8
//13 Sep 2017

//8. In the Stack2 class, add a “peek()” method that only returns the object at the top of the stack
//without removing it. Test this new class.

public class Week2_8 {
    
    //Test driver for 'peek' method
    public static void main(String[] args){
        Stack exerciseStack = new Stack();
      
        for (int i = 1; i < 11; i++){
            exerciseStack.push(i);
        }
        
        System.out.println(exerciseStack.print());
        
        System.out.println("Now lets peek, this is the object on top of Stack : " + exerciseStack.peek());
       
    }    

}