/*
CSIS 2420 Dr. Grady
Dan Taylor
Term Project
20 Nov 2017

14. Implement and illustrate the Heap data structure using arrays from the 
ground up, in a way similar to how we implemented the List and Binary Search 
Tree data structures from the ground up.
*/

public class StringHeap {
    private String heapArray[];
    private int maximumHeapSize;
    private int currentHeapSize;
    
    public StringHeap(int maximiumHeapSize) {
        this.maximumHeapSize = maximiumHeapSize;
        
        currentHeapSize = 0;
        heapArray = new String[maximumHeapSize];
        
    }
    
    public boolean isEmpty() {
        return (currentHeapSize == 0);
    }
    
    public boolean insertItem(String key){
        
        if (currentHeapSize < maximumHeapSize){
            heapArray[currentHeapSize] = key;
            
            //Post increment, will increment the variable after the method call. 
            reHeapifyUp(currentHeapSize++);
            return true;
            
        } else {
            return false;
        }
    }
    
    public void reHeapifyUp(int currentHeapSize){
        int parentIndex = (currentHeapSize-1)/2; //Aka the floor. 
        String child = heapArray[currentHeapSize];
    
        //Pushes the index of lesser priority down. 
        while (currentHeapSize > 0 && 
            (heapArray[parentIndex].compareTo(child) < 0)){  
            heapArray[currentHeapSize] = heapArray[parentIndex]; 
            currentHeapSize = parentIndex;
            parentIndex = (parentIndex-1)/2;
        }
        
        heapArray[currentHeapSize] = child;
    }
    
    //Should replace the top value with the last value, then reheapify
    public String remove() {
        if (isEmpty()){
            return "-1";
        } else {
            String root = heapArray[0];
            heapArray[0] = heapArray[--currentHeapSize];
            reHeapifyDown(0);
            return root;
        }
    }
    
    //Should reheapify when there's something light on top
    public void reHeapifyDown (int startHere){
        int largerchild; 
        String top = heapArray[startHere];
        int leftChild; 
        int rightChild;
        int largerChild;
        
        while (startHere < currentHeapSize/2){
            leftChild = 2*startHere + 1;
            rightChild = 2*startHere +2;
            
            if(rightChild < currentHeapSize && 
                heapArray[leftChild].compareTo(heapArray[rightChild]) < 0){
                largerChild = rightChild;
                
            }else {
                largerChild = leftChild;
            } 
            
            if(top.compareTo(heapArray[largerChild]) > 0){
                break;
            }
            
            heapArray[startHere] = heapArray[largerChild];
            startHere = largerChild;
        }
        heapArray[startHere] = top;
    }
    
    public String[] getHeap () {
        return heapArray;
    }
    
    public void heapSort(String[] stringArrayToInsert){
        Stack stackToReturn = new Stack();
        
        for(int i =0; i < maximumHeapSize; i++){
            reHeapifyUp(currentHeapSize-1);
            Object tempObj = remove();
            stackToReturn.push(tempObj);
        }
        
        System.out.println(stackToReturn.print());
    }
}