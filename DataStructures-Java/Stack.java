/* //Simple Stack
public class Stack extends List {
	public Stack () {
			super("The stack");
	}
	
	public void push (Object obj) {
		super.insertAtFront(obj);
	}
	
	public Object pop () {
		return super.removeFromFront();
	}
	
	public boolean isEmpty (){
		return super.isEmpty();
	}
	
	//public Object peek 
	//	return 
	//
	
	//For testing
	public String print () {
		return super.print();
	}
} */

//But inheritance gives the user too much information, so 
//instead use composition.
//Stack version 2

public class Stack {
	
	private List s;
	
	public Stack () {
		s = new List("The Stack");
	}
	
	public void push (Object obj) {
		s.insertAtFront(obj);
	}
	
	public Object pop () {
		return s.removeFromFront();
	}
	
	public boolean isEmpty (){
		return s.isEmpty();
	}
	
	public Object peek (){
		return s.firstNode.getObject();
	}
	
	//For testing
	public String print () {
		return s.print();
	}
}