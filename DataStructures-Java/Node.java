//Basic Node class
//Java classes contain three main things, field constructors and methods


public class Node {
		private Object data;
		private Node next; //Self referntial, making a Node in a class Node

		public Node (Object obj, Node nextNode){
			data = obj;
			next = nextNode;
		}
		
		public Node (Object obj){
			data = obj;
			next = null;
		}
		
		public void setObject(Object obj){
			data = obj;
		}
		
		public Object getObject(){
			return data;
		}
		
		public void setNext(Node nextNode){
			next = nextNode;
		}
		
		public Node getNext(){
			return next;
		}
}
