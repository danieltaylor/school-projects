//CSIS 2420 Dr. Grady
//Dan Taylor
//Week 3, 2
//18 Sep 2017

//2. Write a QueueWithComposition class using composition, and write an appropriate test 
//driver.  First, test it with the “print” method. Write a “peek()” method that 
//only returns the object at the front of the QueueWithComposition without removing it.

public class Week3_2 {
    public static void main(String[] args){
        
        //Create empty QueueWithComposition with name "Yay, a Queue with Composition"
        QueueWithComposition exerciseQueue = new QueueWithComposition("Yay, a Queue with Composition");
        
        System.out.println(exerciseQueue.print());
        System.out.println("\tIs the exerciseQueue empty? : " + String.valueOf(exerciseQueue.isEmpty()));
        
        //Add an object to the queue
        Object silly = "Silly strings!";
        exerciseQueue.enqueue(silly);
        System.out.println("\nAfter enqueue of 'Silly Strings!, ...");
        System.out.println(exerciseQueue.print());
        
        //Add an object to the queue
        Object sillier = "SILLIER STRINGS";
        exerciseQueue.enqueue(sillier);
        System.out.println("After enqueue of 'SILLIER STRINGS', ...");
        System.out.println(exerciseQueue.print());
        
        //Should return false
        System.out.println("\tIs the exerciseQueue empty? : " + String.valueOf(exerciseQueue.isEmpty()));
        
        //Should return Silly Strings! and false
        System.out.println("\nRemoved " + exerciseQueue.dequeue() + " from the Queue");
        System.out.println("\tIs the exerciseQueue empty? : " + String.valueOf(exerciseQueue.isEmpty()));
        
        //Should return SILLIER STRINGS, and true
        System.out.println("Removed " + exerciseQueue.dequeue() + "fFrom the Queue");
        System.out.println("\tIs the exerciseQueue empty? : " + String.valueOf(exerciseQueue.isEmpty()));
        
        //This should return null, and true
        System.out.println("Removed " + exerciseQueue.dequeue() + " from the Queue");
        System.out.println("\tIs the exerciseQueue empty? : " + String.valueOf(exerciseQueue.isEmpty()));
        
        //Testing peek() method
        
    }
    
   
}
