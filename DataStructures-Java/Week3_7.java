//CSIS 2420 Dr. Grady
//Dan Taylor
//Week 3, 7
//22 Sep 2017

//7. How many memory positions can you access with a 32 bit bus?  What about a 
//64 bit bus? How many colors can be expressed if you have 8 bits for red, 8 for
//green and 8 for blue?

//With a 32 bit bus, there are 2^32 (4,294,967,296) possible memory positions.
//With a 54 bit bus, there are 2^64 (1.8446744e+19) possible memory positions.

//With 8 bits, it is possible to express 2^8 (1024576) shades; therefore with 
// R G B bytes it is possible to express (2^8)^3 or 2^24 (16777216) shades.