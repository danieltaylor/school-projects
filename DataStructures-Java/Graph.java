//CSIS 2420 
//Daniel Taylor


public class Graph {
    private int maxVertices = 20;
    private Vertex[] vertexList;
    private int currentNumVerts;
    private int adjacencyMatrix[][];
    private Edge[] edges;
    private int currentNumEdges;
    
    public Graph () {
        vertexList = new Vertex[maxVertices];
        edges = new Edge[maxVertices];
        adjacencyMatrix = new int[maxVertices][maxVertices];
        
        currentNumVerts = 0;
        currentNumEdges = 0;
        
        //Moves through each index of the matrix, sets each to zero
        for (int i = 0; i < maxVertices; i++){
            for (int k = 0; k < maxVertices; k++){
                adjacencyMatrix[i][k] = 0;
            }
        }
    }
    
    public void addVertex (String s) {
        vertexList[currentNumVerts++] = new Vertex(s);
        
    }
    
    
    public void addEdge (int one, int two, int weight){
        edges[currentNumEdges++] = new Edge(vertexList[one], vertexList[two], weight);
    }
    
    public void displayVertices () {
        System.out.println("\nHere are the vertices: ");
        
        for (int i = 0; i < vertexList.length; i++){
            if (vertexList[i] != null){
                System.out.println(vertexList[i].getLabel());
            }
        }
    }
    
    public void displayEdges () {
        System.out.println("\nHere are the edges: ");

        for (Edge ed : edges){
            if (ed != null)
                ed.print();
        }
    }

}