//Simple Linked List

public class List{
	//Field Section
	public Node firstNode; //Changed this to public from private for Week2_4
	public Node lastNode; //Changed this to public from private for Week2_4
	private String name; 
	
	//constructor
	public List(){
		name = "Default List Name";
		firstNode = lastNode = null;
	}
	
	public List(String s){
		this.setListName(s);
		firstNode = lastNode = null;
	}
	
	public void setListName(String name){
		this.name = name;
	}
	
	public void insertAtFront(Object insertItem){
		if (isEmpty()){
			firstNode = lastNode = new Node(insertItem);		
		} 
		
		else {
			//Create a new node and put data in
			//Point this node to firstNode
			firstNode = new Node(insertItem, firstNode);
			//Rename firstNode
		}
	}
	
	public boolean isEmpty(){
		return (firstNode == null); //If firstNode is null, the list is empty, returns true
	}
	
	public Object removeFromFront(){
		//Could have an empty list
		//Could have only one node
		//Could have more than one node
		
		Object removedItem = null; 
		if (isEmpty()){
			removedItem = "The list is empty, dawg.";
			return removedItem;
		}
		
		removedItem = firstNode.getObject();
		
		if (firstNode.equals(lastNode)){
			firstNode = lastNode = null;
			return removedItem;
			
		} else {
			firstNode = firstNode.getNext();
			return removedItem;
		}
	}
	
	public String print() {
		String result = "";
		String newline = "\n";
		if (isEmpty()){
			result += "Empty List: " + name + newline;
			return result;
		}
		
		result += name + " contains: " + newline;
		Node current = firstNode; 
		
		while (current != null){
			result += current.getObject() + newline;
			current = current.getNext();
		}
		
		return result;
	}
	
	public void insertAtBack (Object insertItem){
		if (isEmpty()) {
			firstNode = lastNode = new Node(insertItem);
			
		} else {
			lastNode.setNext(new Node(insertItem));
			lastNode = lastNode.getNext();
		}
	}
	
	public Object removeFromBack() {
		
		Object removedItem = null;
		//List could be empty
		if (isEmpty()) {
			removedItem = "But this list is empty!";
			return removedItem;
		}
		
		removedItem = lastNode.getObject();
		
		//List could have only one node
		if (firstNode.equals(lastNode)){
			System.out.println("First node equals last node");
			firstNode = lastNode = null;
			return removedItem;
		}
		
		//Could have more than one node
		else {
			Node secondLast = firstNode;
			
			while (secondLast.getNext() != lastNode){
				secondLast = secondLast.getNext();
			}
			secondLast.setNext(null);
			lastNode = secondLast;
			
			return removedItem;
		}
	}
	
	public int getListSize() {
		Node current = firstNode;
		int count = 0;
		
		if (isEmpty()){
			return count;
		}
		
		while (current != null){
			count++;
			current = current.getNext();
		}
		return count;
	}
}
