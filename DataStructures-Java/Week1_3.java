//CSIS 2420 Dr. Grady
//Dan Taylor
//Week 1, 2
//Using only the Node class, build a linked list that stores all the squares from 1 to 100.  Then migrate through the list and print out the squares.  


public class Week1_3 {

	
	public static void main(String[] args) {
		Node x = new Node(100*100);
		
		
		for (int i = 99; i > 0 ; i--){
			x = new Node((i*i), x);
		}
		
		for (int i = 1; i <= 100; i++){
			
			System.out.println("The square of " + i + " is " + x.getObject());
			
			if(x.getNext()!=null){
				x = x.getNext();
			}
		}
	}
}