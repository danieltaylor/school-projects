//CSIS 2420 Dr. Grady
//Dan Taylor
//Week 2, 4
//13 Sep 2017

public class Week2_4 {
	
	//Have the last node of the List in ex. 3 point to the first node. What happens when you try to
	//print the list? (In your web page, you just need to show the whole list printed twice).
	public static void main(String[] args){
		List exerciseList = new List("Week 2 Exercise 4");
		
		for (int i = 1; i <= 100; i++){
			exerciseList.insertAtFront(i*i);
		}
		
		Node first = exerciseList.firstNode;
		Node last = exerciseList.lastNode;
		//Have the last node of the list point to the first node
		last.setNext(first);
		//System.out.println(exerciseList.lastNode.getNext());
		//System.out.println(exerciseList.firstNode.getNext());
		
		System.out.println(exerciseList.print());
	}
}
