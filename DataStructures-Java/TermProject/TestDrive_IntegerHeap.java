/*
CSIS 2420 Dr. Grady
Dan Taylor
Term Project
20 Nov 2017

14. Implement and illustrate the Heap data structure using arrays from the 
ground up, in a way similar to how we implemented the List and Binary Search 
Tree data structures from the ground up.
*/

public class TestDrive_IntegerHeap {
    public static void main(String[] args) {
        System.out.println("\n...Creating a new heap of " + args[0] + 
            " randomly generated numbers...");
            
        int newHeapSize = Integer.parseInt(args[0]); //Cast String to int
        IntegerHeap newHeap = new IntegerHeap(newHeapSize);
        
        while (newHeapSize > 0){ //Loop exits when Heap full
            
            //Generates random integers while Heap !full
            newHeap.insertItem((int) Math.floor(Math.random() * 1001));
            newHeapSize--;
        }
        
        //Removes elements until heap is empty
        System.out.println("...Printing out things as they're removed...");
        while (!newHeap.isEmpty()) {
            System.out.println(newHeap.remove());
        }
    }
}