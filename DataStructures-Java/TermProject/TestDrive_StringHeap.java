/*
CSIS 2420 Dr. Grady
Dan Taylor
Term Project
20 Nov 2017

14. Implement and illustrate the Heap data structure using arrays from the 
ground up, in a way similar to how we implemented the List and Binary Search 
Tree data structures from the ground up.
*/
import java.util.*;

public class TestDrive_StringHeap {
    public static void main(String[] args) {
        System.out.println("\n***Creating a nice Thanksgiving heap***");
        int newHeapSize = Integer.parseInt(args[0]); 
        StringHeap newHeap = new StringHeap(newHeapSize);
        Scanner in = new Scanner(System.in);
        
        insert("turkey", newHeap);
        insert("cranberry sauce", newHeap);
        insert("mashed potatoes", newHeap);
        insert("stuffing", newHeap);
        insert("some dessert", newHeap);
        
        System.out.println("\n***Removing things from Thanksgiving heap***");
        while (!newHeap.isEmpty()){
            String entry = prompt();
            if (entry.equals("")){
            }else{
                insert(entry, newHeap);
            }
            removeMin(newHeap);
        }
        
        System.out.println("***Heap demo over! Thanks!***\n");
    }
    
    public static void removeMin(StringHeap heap) {
        System.out.println("Just removed: " + heap.remove() + "\n");
    }
    
    public static String prompt(){
        Scanner in = new Scanner(System.in);
        String response = in.nextLine();
        return response;
    }
    
    public static void insert(String toInsert, StringHeap inHeap){
        inHeap.insertItem(toInsert);
        System.out.println(makePretty(toInsert));
    }
    
    public static String makePretty(String toInsert){
        int numSpaces = 20 - toInsert.length();
        String spaces = "";
        for (int i = 0; i < numSpaces; i++){
            spaces = spaces + ".";
        }
        String silly = "\tAdd to heap" + spaces + toInsert;
        return silly;
    }
}
