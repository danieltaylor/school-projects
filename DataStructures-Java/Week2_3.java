//CSIS 2420 Dr. Grady
//Dan Taylor
//Week 2, 3
//13 Sep 2017


//3. Create a list and fill it with the squares from 1 to 100, then print out the contents.
public class Week2_3 {
	
	
	public static void main(String[] args){
		List exerciseList = new List("Week 2 Exercise 3");
		
		for (int i = 1; i <= 100; i++){
			exerciseList.insertAtBack(i*i);
		}
		
		System.out.println(exerciseList.print());
	}
}