//CSIS 2420 Dr. Grady
//Dan Taylor
//Week 2, 5
//13 Sep 2017

import java.math.*;

//5. Add and test a method for the List class which returns the size of the list (i.e. the number of
//elements in the list).
public class Week2_5 {
    
    //Test driver to get size of list
    public static void main(String[] args){
        List exerciseList = new List("Week2_5 list");
        	//Four elements we used in class?
    	Character ch = new Character('$');
    	BigInteger bi1 = new BigInteger("111111111111111111111111111111");
    	BigInteger bi2 = new BigInteger("222222222222222222222222222222");
	    BigInteger bi3 = new BigInteger("333333333333333333333333333333");
	    

	    //Add elements one at a time and print what's in the list. 
	    System.out.println(exerciseList.print());
	    exerciseList.insertAtBack(ch);
	    System.out.println(exerciseList.print());
	    exerciseList.insertAtBack(bi1);
	    System.out.println(exerciseList.print());
    	exerciseList.insertAtBack(bi2);
    	System.out.println(exerciseList.print());
    	exerciseList.insertAtBack(bi3);
    	System.out.println(exerciseList.print());
    	System.out.println("This list contains: "  + exerciseList.getListSize() + " nodes");
        
    }
}