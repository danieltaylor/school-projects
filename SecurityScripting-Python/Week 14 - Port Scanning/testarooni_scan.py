#Dan Taylor 
#17 April 2017
#Port Scanner testarooni

import socket, sys, optparse

#Creates and adds options
parsearooni = optparse.OptionParser('usage testarooni_scan.py -H ' +  
									'<target host> -p <target ports, '+
									'seperated by spaces>')
parsearooni.add_option("-H", "--host", type='string',
						dest = 'tgtHost',  
						help = "The target host")
parsearooni.add_option("-p","--port", 
						dest = 'tgtPorts',
						type = 'string',
						help = "Input target ports seperated by spaces", 
						metavar = "Port")
	
(options, args) = parsearooni.parse_args()

#Creates local variable
tgtHost = options.tgtHost
tgtPorts = str(options.tgtPorts).split(', ')
if (tgtHost == None)| (tgtPorts == None):
	print(parsearooni.usage)
	exit(0)

#Attempts to connect to the host at the specified port.
def connectScan(tgtHost, tgtPort):
	try:
		connectSocket = socket(AF_INET, SOCK_STREAM)
		connectSocket.connect((tgtHost, tgtPort))
		print ('[+]%d/tcp open'% tgtPort)
		connectSocket.close()
	except:
		print ('[-]%d/tcp closed'% tgtPort)
		
#Prints the results	
def portScan(tgtHost, tgtPorts):
	try:
		print ('\n[+] Scan results for: ' + tgtHost)
	except:
		print('\n[+] Scan results for: ' + tgtIP)
	for port in tgtPorts:
		print('Scanning port' + port)
		connectScan(tgtHost, int(port))
		
portScan(tgtHost, tgtPorts)

