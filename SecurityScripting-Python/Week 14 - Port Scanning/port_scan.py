
#Dan Taylor 
#17 April 2017
#Port Scanner testarooni
from socket import *
from threading import *
screenLock = Semaphore(value=1)
import optparse

#Creates and adds options
parsearooni = optparse.OptionParser('usage testarooni_scan.py -H ' +  
									'<target host> -p <target ports, '+
									'seperated by commas>')
parsearooni.add_option("-H", "--host", type='string',
						dest = 'tgtHost',  
						help = "The target host")
parsearooni.add_option("-p","--port", 
						dest = 'tgtPorts',
						type = 'string',
						help = "Input target ports seperated by commas", 
						metavar = "Port")

(options, args) = parsearooni.parse_args()

#Creates local variable
tgtHost = options.tgtHost
tgtPorts = str(options.tgtPorts).split(',')

#Prints usage if user didn't do it right
if (tgtHost == None)| (tgtPorts == None):
	print(parsearooni.usage)
	exit(0)

#Attempts to connect to the host at the specified port.
def connectScan(tgtHost, tgtPort):
	try:
		connectSocket = socket(AF_INET, SOCK_STREAM)
		connectSocket.connect((tgtHost, tgtPort))
		connectSocket.send('Yo')
		results = connectSocket.recv(100)
		screenLock.acquire()
		print('%d/tcp open'% tgtPort)
		print(str(results))
	except:
		print ('[-]%d/tcp closed'% tgtPort)
	finally:
		screenLock.release()
		connectSocket.close()
		
#Prints the results	
def portScan(tgtHost, tgtPorts):
	try:
		print ('\n[+] Scan results for: ' + tgtHost)
	except:
		print('\n[+] Scan results for: ' + tgtIP)
	for port in tgtPorts:
		print('Scanning port' + port)
		connectScan(tgtHost, int(port))
	setdefaulttimeout(1)
	for tgtPort in tgtPorts:
		t = Thread(target = connectScan, args=(tgtHost, int(tgtPort)))
		t.start()
		


#Runs the scan using the arguments given		
portScan(tgtHost, tgtPorts)







