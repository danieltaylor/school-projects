#Dan Taylor

file_name = 'write_hello.txt'

#with open(file_name, 'w') as f:
	#f.write("Hello Wurld!")
	#f.write('Goodbye Wurld')
	#If you use .write(), if the file exists it writes over it, and if
	#it doesn't exist it just creates the file. 
	

#with open(file_name, 'a') as f:
	#f.write("Hello Wurld!")
	#f.write('Goodbye Wurld')
	#With the a option, it appends to the file. 
	#After you're done with this script, python automaticall closes the 
	#file. This doesn't add lines, it justs adds the text. 

with open(file_name, 'w') as f:
	hello = 'Hello world!\n'
	f.write(hello)
	f.write('Goodbye Wurld\n')
	f.writelines('Hi Poly')
	#Write lines takes an iterable, what is that???

with open(file_name, 'w') as f:
	hello_list = ['Hello world!\n', 'Goodbye Wurld2', 'Yo Pollyyyyy']
	f.writelines(hello_list)
	#Keep in mind what happens when you use these different functions. 
	#for the last 10 minutes, create a module 'file_io.py'
