#Dan Taylor
#CSIS 4850 
#Assignment 4

#6.1 Person creates dictionary about me and prints. 
print('Behold, a dictionary of Dan information')

dan_dictionary = {
	'first name':'Dan',
	'last name' : 'taylor',
	'age' : 21,
	'city' : 'rancho cucamonga'
}
for key, value in dan_dictionary.items():
	print("\nInfo type: " + key)
	print("Info: "+value)






#6.3 creates a gloassary of terms I have learned. Also prints the 
#dictionary with explanatory statements. 
glossary = {
	'tuple' : 'A variable that cannot be changed',
	'blank space' : 'Means something to Python',
	'key' : 'The thing wot is to the left',
	'value' : 'This is a value',
	'dictionary' : 'In python, key value paired.'
}

print("Behold, a glossary of five things Dan learned in this class.\n")

for key, value in glossary.items():
	print("\nKey new Word: "+ key)
	print("Value: " + value)





#6.7 creates a list of dictionaries of people, then prints them
#according to instructions. 
dan_dictionary = {
	'first name':'Dan',
	'last name' : 'taylor',
	'age' : 21,
	'city' : 'rancho cucamonga'
}

bill_dictionary = {
	'first name' : 'Bill',
	'last name' : 'Murray',
	'age' : 67,
	'city' : 'Evanston'
}

frosty_dictionary = {
	'first name' : 'Frosty',
	'last name' : 'theSnowMan',
	'age' : 0,
	'city' : 'Everytown' 
}

people = [dan_dictionary, bill_dictionary, frosty_dictionary]

for key, value in dan_dictionary.items():
	print("Dan's "+ key + " is "+ value)
print("\n")
for key, value in glossary.items():
	print("Bill Murray's " + key + " is " + value)
print("\n")
for key, value in frosty_dictionary.items():
	print("\nFrost's "+ key + " is " + value)
	
	


	
#6.8 Creates a dictionary that holds credit card info, with the key being
#the customers last name, the value being the CC number, expiration date, 
#and the CVV. The credit card information should not be editable or 
#changeable. Your dictionary should have at least 4 entries. Create a loop 
#that prints all the information in the dictionary. Format so it is 
#human readable and make sure you print statements that explain what is 
#being displayed.

credit_cards ={
	'may': ('4358907089916666', '01/2017', '840'),
	'trump': ('4358123457860981', '2/2018', '666'),
	'netanyahu': ('90906772991183', '3/2019', '777'),
	'putin' : ('4444333322221111', '4/2020', '888')
}
 
for key, value in credit_cards.items():
	print("Head of state " + key + "'s Credit card number, expiration date, and CVV is " )
	for each in value:
                print(each)
