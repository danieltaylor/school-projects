#Dan Taylor
#8 March 2017
#File Input / Output




def file_io(file_name, mode, *text):
	"""Writes and reads from files according to instructions"""
	
	
	#Printed statements.
	bad_mode = "Invalid mode. Use 'r' for read-only, 'r+' for read-and-write"
	bad_mode += ", 'a' for append, or 'w' for write."
	
	
	
	
	#Checks if the file exists.
	try:
		with open(file_name) as f:
			pass #If there is a file with that name, do nothing.
			
	except IOError as e:
		print("File does not exist, or you do not have permissions.")
	
	
	
	#Determines which block of code to run based on input.
	if (mode == 'r+'):
		with open(file_name, 'r+') as read_write_object:
			
			if(text):
				for line in text:
					read_write_object.write("%s\n" % line)
					
			line_of_text_read = read_write_object.readlines()
			
			for line_of_text_read in read_write_object:
				print(line_of_text_read)	
					
					
	elif (mode == 'r'):	
		with open(file_name) as read_file:
			
			if (text): #Text should not be passed here.
				print('Text cannot be written in read-only mode.')
				
			contents = read_file.read()
			print(contents)	
			
	elif (mode == 'w'):
		with open(file_name, 'w') as write_file:
			
			for line in text:
				write_file.write("%s\n" % line)
				
	elif (mode == 'a'):
		if (text): #Append should only write if text is passed.
			with open(file_name, 'a') as append_file:
				
				if(text):
					for line in text:
						append_file.write("%s\n" % line)
			
	else:
		print(bad_mode)

