import logging
#Keyword argument below where keyword is level 

#logging.basicConfig(filename='logfile.log', level=logging.DEBUG)
logger = logging.getLogger()
#This returns us a logger set at debug level.

#Commented uot the basicConfig. Creating a handler
fh = logging.FileHandler('logger_file.log')
fh.setLevel(logging.WARNING) #this is default but yolo
sh = logging.StringHandler()
sh.setLevel(logging.DEBUG)

logger.addHandler(fh)
logger.addHandler(sh)



#To use this thing, we do 
logger.debug("This message is for debugging")
logger.error("This is bad")
logger.critical("DANGER WILL ROBINSON")

#After you are done, you only need to comment out this line to get rid 
#of debug messages. However, default level is warning, so, 
#we will still get error and critical. 
#logging.basicConfig(level=logging.DEBUG)

#What is a handler?

#Handlers allow you to choose where you want to send things basically

#If we set up the handlers correctly, we should see debug statements to 
#the console, and warning or higher messages will go the log file. 

#Why does this matter? Let's look at Simple Calc class. 
#We can log what the user is passing in when they call our functions.
