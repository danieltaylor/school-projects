#Dan Taylor
#Allow for user to pass file name, mode (read, write, etc), third 
#argument that will allow you to take in all the things you want to 
#write. You should probably take in something that will take in a list, 
#no matter how many they want to write. 

#Wednesday we'll talk about the midterm project.

from file_utils import file_io

file_io("test_file.txt","w","This is a test.")
file_io("test_file.txt",'r')
file_io("test_file.txt",'r')
file_io("test_file.txt","w")
file_io("test_file.txt",'r')
file_io("test_file.txt","a", "Another test.", "And another.", "One more.")
file_io("test_file.txt","a")
file_io("test_file.txt",'r')
file_io("test_file.txt", 'r+', 'Last test.')
file_io("test_file.txt",'r+')
file_io("test_file.txt", 'r+')
file_io("test_file.txt",'r-')
