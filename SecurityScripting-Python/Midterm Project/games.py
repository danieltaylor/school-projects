#Dan Taylor

#imports
from random import randint
import sys

class NumberGame():
	"""Log in to play a number guessing game with three difficulties"""
	
	logged_in = False
	difficulty = 'not'
	b = False
	score = 0
	  
	def __init__(self, username):
		self.username = username
		self.login()
		
	def login(self):
		"""Process to log a user into games"""
		
		#Printed statements
		user_list_file_error = "User list does not exist or you do not "
		user_list_file_error += "have permissions. You cannot log in."
		
		new_profile_prompt = "No user by that name. Enter 'new' to "
		new_profile_prompt += "create a new profile, or enter 'quit' "
		new_profile_prompt += "to exit without creating a user. "
		
		new_passw_prompt = 'Enter a password for the new account'
		
		b = False
		
		try:
			with open('user_list.txt') as f:
				
				#Opens text file, strips white space, adds into list
				user_list = f.readlines()
				user_list = list(map(str.strip, user_list))
				
				#Takes lines in as user or password lines
				users = user_list[0::2]
				passwords = user_list[1::2]
				
				f.close()
				
		except IOError:
				print(user_list_file_error)
				sys.exit(0)
			
		password_check = False
		
		if self.username in users:
			
			while password_check == False:
				
				try:
					input_password = input(self.username + 
										", enter your password:")
				except:
					sys.exit()
											
				#Checks if user's password matches password in file
				if (input_password == 
					passwords[users.index(self.username)]):
						
					password_check = True
					self.logged_in = True
					self.b = True
					return 
				else:
					print(self.username.title() + 
							', that is not your password.' + 
							' Please try again.')
					continue
		else: #Choice to create an account
			
			try:
				profile_choice = False
				
				while profile_choice == False:
					
					with open('user_list.txt', 'a') as ef:
						
						new_profile_choice = input(new_profile_prompt)
							
						if(new_profile_choice.lower() == 'new'):
							
							try:
								new_password = input(new_passw_prompt)
							except:
								sys.exit()
								
							#Writes the users new password
							ef.write(self.username+'\n')
							ef.write(new_password+'\n')
							profile_choice = True
							
						elif(new_profile_choice == 'quit'):
							print("\nDon't want a profile?")
							print("Bye then, " + self.username)
							profile_choice = True
							sys.exit()
							
						else:
							print("Enter 'new' or 'quit'")
				
			except IOError as e:
				print(user_list_file_error)	
			except KeyboardInterrupt:
				print('Bye '+ self.username)
				sys.exit()		
	
	def set_difficulty(self, difficulty):
		"""Sets the difficulty of the Number Game"""
		
		if (self.logged_in == True):
			
			#Checks, then sets difficulty accordingly
			if(difficulty == 'easy'):
				self.difficulty = 'easy'
			elif(difficulty == 'medium'):
				self.difficulty = 'medium'
			elif(difficulty == 'hard'):
				self.difficulty = 'hard'
			elif(difficulty == 'quit'):
				print('See ya later, %s!' % self.username)
				self.b = False
				sys.exit(0)
				
		else:
			self.login()
			self.set_difficulty(difficulty)
			self.play()
			
	def play(self):
		"""Plays the number game with pre-set username and difficulty"""
			
		if(self.logged_in == False):	
			print('You cannot play because you are not logged in.')
			self.login()
		
		
			
		if self.b: #Checks if user has attempted log in.
			pass
		else:
			sys.exit()
		
		
		#Printed statements 
		win = 'Congratulations, you have won. Rematch.\n\n'
		out_of_bounds = 'You guessed a number outside this difficulty range.'
		not_int = 'Enter an integer between 1 and '
		low_guess = 'That number is too low.'
		high_guess = 'That number is too high.'
		invalid_mode = 'That is not a difficulty mode. Enter "easy", "medium", '
		invalid_mode += 'or "hard" without quotations.\n'
		guessed_to_win = 'Congrats. You guessed the number on try: '
		quitter = "You're a quitter after try: "
		taunt = 'Try again, wimp.'
		taunt_two = "Is that all you've got?"
		taunt_three = "Why do you keep guessing bad guesses?"
		thanks = '. Thanks for playing!'
		
		#Initialize loop conditions
		thegame = True
		thisgame = True
			
		#While loop to run until quit
		
		while thegame == True:
			
			#Establish range to guess in
			if (self.difficulty == 'not'):
				print('Set difficulty before playing.')
				self.prompt_for_difficulty()
			
			if (self.difficulty == 'easy'):
				guess_range = 10
				
			elif (self.difficulty == 'medium'):
				guess_range = 100
				
			elif (self.difficulty == 'hard'):
				guess_range = 1000
				
			#Prompt user to select easy, medium, or hard mode
			prompt = ('Guess integers 1- '+ str(guess_range) +
						', or enter "quit" w/o quotations.\n')
			
			int_to_guess = randint(1, guess_range)
			guesses = 0 #Keeps track of number of guesses
			
			while thisgame == True:
				
				won = False
				
				if (guesses == 0):
					try:
						guessed_int = input(prompt) #User guessed int
					except:
						continue
						
				elif(guesses > 0 & guesses < 3):
					try:
						guessed_int = input('')
					except:
						guessed_int = 0
						print('Bad Guess')
				elif(guesses >= 3):
					try:
						guessed_int = input(taunt)
					except:
						guessed_int = 0
						print('Bad Guess')
					
				if guessed_int == 'quit':
						thisgame = False
						thegame = False
						print('See ya later, %s!' % self.username)
						sys.exit()
						
							
				#Avoid crashing with try-except 
				try:
						guessed_int = int(guessed_int) #casts input as int
						
						if guessed_int == int_to_guess:
							guesses+=1
							won = True
							self.score = guesses
							break
						
						elif guessed_int < 0 or guessed_int > guess_range:
							guesses+=1
							print(out_of_bounds + str(guess_range))
							
						elif guessed_int < int_to_guess:
							guesses+=1
							print(low_guess)
							
						else:
							guesses+=1
							print(high_guess)
					
				except:	
					guesses+=1
					print(not_int + str(guess_range)) #Lets the user know they entered non-int
					continue
					
						
			#Prints the number of times the user guessed
			if not won:
				
				print(quitter+str(guesses)+thanks)
				
			else:
				
				print(guessed_to_win+str(guesses)+thanks)
				rematch = input("\nEnter 'new' for a rematch, or" + 
								" anything else to quit")
					
				if(rematch.lower() == 'new'):
					self.difficulty = 'not'
					continue
					
				else:
					sys.exit()
		
		
	def score(self):
		"""Collects and returns user score"""
		return score
		
	def prompt_for_difficulty(self):
		"""Prompts the user for a difficulty level"""
			
		prompt = '\nPlease enter "easy", "medium", "hard" or "quit"'
		prompt += ' without quotations.\n'
		
		prompt_response = False #Asks until user responds properly
		
		while not prompt_response:
			
			try:
				prompt_response = input(prompt)
				self.set_difficulty(prompt_response)
				prompt_response = True
			except:
				continue
		
		
