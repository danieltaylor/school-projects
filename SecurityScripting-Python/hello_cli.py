import optparse

parser = optparse.OptionParser()

#Default value for this option is 'world')
#metavar is the meta variable
parser.add_option("-n", "--name", default='world', dest = 'name', 
	help = "Say hello to NAME", metavar= "Namen.") 

#Its going to return?? Use the parser you just made
(options, args) = parser.parse_args()

#Now we can use the options we just parsed by printing Hello World. 
print("Hello %s!" % options.name)
