#Dan Taylor
#Logic Calculator
#3 Feb 2017




choices = 1
#Easily let the user run again. 
while (choices==1):
	prompt = "This simple logic calculator will ask you for two numbers,"
	prompt += " and then compare them."
	var_prompt = "\nEnter the first number: "
	var_prompt_two = "\nEnter the second number: "
	
	
	var = input(prompt+var_prompt)
	#Checking user input with input to ensure try-except works in 
	#python 2.7
	try:
		int(var)
	except ValueError:
		print("That's not an integer.")
		continue
	

	
	
	var_two = input(var_prompt_two)
	#checking user input, usings input because Python 2.7 is weird
	try:
		int(var_two)
	except ValueError:
		print("That's not an integer.")
		continue
	
	
	
	#The logic part
	if(var>var_two):
		print("The first number is bigger.")
	elif(var==var_two):
			print("They are the same number.")
	elif(var_two>var):
		print("The first number is smaller than the second number.")
	else:
		print("Try again, you didn't enter two integers.")
	
	
	#Again, using raw_input to ensure the try-except works in python 2.7	
	choices = input("Enter 1 to run again, or anything else to quit")
	if(choices != 1):
		try:
			int(choices)
		except:
			ValueError
			raise SystemExit
	
