def say_hello(first='world', last=""):
    """Say hello to someone"""
    if last:
        print("Hello %s %s!" % (first.title(), last.title()))
    else:
        print("Hello %s!" % first.title())

def say_bye(first='world', last=""):
	"""Say bye to someone"""
	if last:
		print("Bye %s %s!" % (first.title(), last.title()))
	else:
        print("Bye %s!" % first.title())
		
