from student_info import Student

yolo_student = Student('Dan','Taylor', 12345)
yolo_student.set_status('Ugly')
yolo_student.set_year('Junior')
yolo_student.set_courses('Security Scripting', 'Everything else')
yolo_student.set_club_membership(['CDCC', 'Pacesetters'])
yolo_student.set_cool_factor(0)

print(yolo_student.get_fullname())
print(yolo_student.get_id_number())
print(yolo_student.get_year())
print(yolo_student.get_course_list())
print(yolo_student.get_status())
print(yolo_student.get_club_membership())
print(yolo_student.get_cool_factor())

print('\n\n\n')

yol_student = Student('D','T', 15)
yol_student.set_status('active')
yol_student.set_year('sophomore')
yol_student.set_courses('Scripting', 'Everything')
yol_student.set_club_membership(['CDCCaaaaa', 'Pacesettersaaaaaa'])
yol_student.set_cool_factor(10)

print(yol_student.get_fullname())
print(yol_student.get_id_number())
print(yol_student.get_year())
print(yol_student.get_course_list())
print(yol_student.get_status())
print(yol_student.get_club_membership())
print(yol_student.get_cool_factor())


print('\n\n\n')

yo_student = Student('That','Guy', 999999)
yo_student.set_status('   ')
yo_student.set_year('Junior77')
yo_student.set_courses('Security Scripting', 'Everything else')
yo_student.set_club_membership(['CDCC', 'Pacesetters'])
yo_student.set_cool_factor(77)

print(yo_student.get_fullname())
print(yo_student.get_id_number())
print(yo_student.get_year())
print(yo_student.get_course_list())
print(yo_student.get_status())
print(yo_student.get_club_membership())
print(yo_student.get_cool_factor())
