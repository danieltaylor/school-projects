#Dan Taylor
#Exercise 2-7
#Code to store whitespaced string and print using strip methods to practice

name = "\n\t\t  yo'olo baggins  \n   \t    "

print(name)

#print name after hitting it with the lstrip
print('After lstrip-->'+name.lstrip())

#print name, after rstrip
print('After rstrip-->'+name.rstrip())

#print name, after strip
print('After strip-->'+name.strip())

#print name, after title and strip
print('Pretty: '+name.title().strip())
