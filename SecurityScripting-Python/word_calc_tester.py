#Dan Taylor
#27 Feb 2017
#Word Calc Tester

from word_calc import adder


#Four test cases. First test has a number word which is out of range. 
#Second test has proper usage. 
#Third test has misspelled number words.
#Fourth test has a numeric character instead of a number word.

test_one = adder('five','eleven')
print(test_one)

test_two = adder('zero','five')
print(test_two)

test_three = adder('thhree','sven')
print(test_three)

test_four = adder('one',1)
print(test_four)
