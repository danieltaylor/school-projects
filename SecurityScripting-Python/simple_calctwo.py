#Dan Taylor
#27 Feb 2017
#Word Calculator

def __init__(self):
	pass
	
def adder(a, b):
	"""Add two integers in range 0-10, all int represented by strings"""
	
	#Printed statements
	bad_input = 'Detected non alphabetical characters. \n >Please '
	bad_input += 'enter two integers, 0-10, as "one", "two", etc.'
	
	not_number = 'Detected all alphabetic characters, but no number ' 
	not_number += 'words were found. \n >Please enter two integers, 0-10,'
	not_number += ' as "one", "two", etc.'
	
	#Try - except to prevent crashing from user error
	try:
		number_words = {
		'zero':0,
		'one':1,
		'two':2,
		'three':3,
		'four':4,
		'five':5,
		'six':6,
		'seven':7,
		'eight':8,
		'nine':9,
		'ten':10,
		}
		
		#input is passed through lower(), if not a string, it throws 
		#AttributeError
		a = a.lower()
		b = b.lower()
		
		#Checks if both arguments are words, then adds and returns as
		#a string
		if a in number_words and b in number_words:
			
			added = number_words[a]+ number_words[b]
			return(str(added))
			
		else:
			
			return(not_number)
			
	except AttributeError:
		return(bad_input)
	except SyntaxError:
		return(bad_input)
		
		
	
		
	
