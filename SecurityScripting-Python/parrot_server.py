#Dan Taylor
#10 April 2017
#Socket Redux


import socket, sys, optparse

#Creates and adds options
parsearooni = optparse.OptionParser()
parsearooni.add_option("-s", "--server_name", default = "localhost", 
	dest = 'server', help = "Server default is localhost, port 4444")
parsearooni.add_option("-p", "--port", default = 4444, dest = 'port', 
	help = "Port to communicate on", metavar = "Port")
	
(options, args) = parsearooni.parse_args()


#Creates a socket with ipv4, STREAM connection until terminated.  
socker = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

#Defines local server address, listens on that address
svr_addr = (options.server, int(options.port))
socker.bind(svr_addr)
socker.listen(1)

print("Listening on: " + str(svr_addr[0])+" port: "+str(svr_addr[1]))


while True:
	
	#Prints connection info
	connection, client_addr = socker.accept()
	print("Connection from " + str(client_addr[0]) + ":" + 
		str(client_addr[1]))
		
	try:
		
		while True:
			
			#Data from socket test
			data = connection.recv(1024)
			
			if data:
				
				#Prints recieved data
				print("Recieved data from " + str(client_addr[0]) + ":" 
					+ str(client_addr[1]))
				connection.sendall(data)
				print("\t"+str(data))
				
			else:
				break
				
	finally:
		connection.close()
