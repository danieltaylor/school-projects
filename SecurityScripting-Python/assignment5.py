#You are going to ADD two words. Not concatenate. 
#call them whatever you want. Take in two words. 
#The arguments are strings that represent numbers, eg one two three
#We should have two test cases where it gives good results.
#We should have two test cases where it returns bad results. 
#Make sure in your function in your module you do the """ """


from assignment5 import adder

result = adder('one', 'two')

print('result') #Should return 'three'
