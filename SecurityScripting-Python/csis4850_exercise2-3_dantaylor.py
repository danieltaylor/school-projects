#Dan Taylor
#Exercise 2-3, Python Crash Course
#Code to print a greeting to a name from a variable. 

name = 'cave johnson'
print('Welcome back, '+name.title()+'.')
