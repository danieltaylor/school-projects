class Student():
	"""Store and access student information"""
	status = 'Error in records'
	year = 'Error in records'
	cool_factor = 'Error in records'
	def __init__(self, first_name, last_name, student_id_number):
		"""Initializes student with first, last names, academic year"""
		self.f_name = first_name
		self.l_name = last_name
		self.id_number = student_id_number
	
	def set_courses(self, *list_of_courses):
		"""Sets student schedule to the input list"""
		self.course_list = []
		
		#Difficult to check for user input here, funky course names
		for course in list_of_courses:
			self.course_list.append(course)
		
	def set_status (self, student_status):
		"""Sets student status, eg. good bad or ugly"""
		try:
			if(student_status.lower() == 'good'):
				self.status = student_status
			elif (student_status.lower() == 'active'):
				self.status = student_status
			elif (student_status.lower() == 'bad'):
				self.status = student_status
			elif (student_status.lower() == 'ugly'):
				self.status = student_status
		except AttributeError:
			return "Input not 'good, 'bad', or 'ugly'"
		
	def set_year(self, student_academic_year):
		"""Sets Student Academic year to input value, eg. freshman"""
		years = ('freshman', 'sophomore', 'junior', 'senior')
		if (student_academic_year.lower() in years):
			self.year = student_academic_year
		
	def set_club_membership(self, student_club_membership):
		"""Sets students club membership to input list"""
		self.club_membership = []
		for club in student_club_membership:
			self.club_membership.append(club)
		
	def set_cool_factor(self, student_cool_factor):
		"""Sets student cool factor to an int, 0-10"""
		try:
			if(int(student_cool_factor) <= 10 & int(student_cool_factor) >= 0):
				self.cool_factor = student_cool_factor
		except ValueError:
			return "Not an int 0-10"
			
	def get_fname(self):
		"""Returns first name"""
		return f_name.title()
	
	def get_lname(self):
		"""Returns last name"""
		return l_name.title()

	def get_fullname(self):
		"""Returns student's full name"""
		return self.f_name.title()+ " " + self.l_name.title()
	
	def get_id_number(self):
		"""Returns student's ID number"""
		return self.id_number
	
	def get_course_list(self):
		"""Returns student's course list"""
		return self.course_list
	
	def get_status(self):
		"""Return student's status"""
		return self.status
		
	def get_year(self):
		"""Return student's academic year"""
		return self.year
		
	def get_club_membership(self):
		"""Returns a list of student club membership"""
		return self.club_membership
	
	def get_cool_factor(self):
		"""Returns student's cool factor"""
		return self.cool_factor
