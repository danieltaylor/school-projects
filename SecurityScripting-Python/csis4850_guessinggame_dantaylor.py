

#Prompt user for input, store as string 'guessed'
badass_prompt = 'Welcome to the badass guessing game for badasses.'
badass_prompt += '\nPlease enter "easy", "medium", "hard" or "quit"'
badass_prompt += ' without quotations.\n'



#Prompt user to select easy, medium, or hard mode
easy_prompt = 'Guess integers 1-10, or enter "quit" w/o quotations.\n'
medium_prompt = 'Guess integers 1-100, or enter "quit" w/o quotations.\n'
hard_prompt = 'Guess integers 1-1000, or enter "quit" w/o quotations.\n'


#Printed statements 
win = 'Congratulations, you have won. Rematch.\n\n'
out_of_bounds = 'You guessed a number outside this difficulty range.'
not_int = 'Enter an integer within this difficulty range, eg 5.'
low_guess = 'That number is too low. Try again, wimp'
high_guess = 'That number is too high. Try again, wimp'
invalid_mode = 'That is not a difficulty mode. Enter "easy", "medium", '
invalid_mode += 'or "hard" without quotations.\n'
guessed_to_win = 'Congrats. You guessed the number on try: '


#Initialize loop conditions
thegame = True
thisgame = True

    def play(self, mode)
		"""The number game"""
		
		int_to_guess = randint(
		
		


#While loop runs the game until they quit at main menu
while thegame == True:
	
	mode = input(badass_prompt) #Takes user input as difficulty mode
	
	if mode.lower() == 'easy':
		
		int_to_guess = randint(1,10) #Generates random int in range
		guesses = 0 # Keeps track of number of guesses
		
		
		#While loop to run until quit
		while thisgame == True:
			
			if (guesses == 0):
				guessed_int = input(easy_prompt) #User guessed int
			elif(guesses > 0):
				guessed_int = input('')
			
			#Avoid crashing with try-except 
			try:
				if guessed_int == 'quit':
					break
					
				guessed_int = int(guessed_int) #casts input as int
				
				if guessed_int == int_to_guess:
					guesses+=1
					print(win)
					
					#Prints the number of times the user guessed
					print(guessed_to_win+str(guesses)+"\n") 
					break
				elif guessed_int < 0 or guessed_int > 10:
					guesses+=1
					print(out_of_bounds)
				elif guessed_int < int_to_guess:
					guesses+=1
					print(low_guess)
				else:
					guesses+=1
					print(high_guess)
					
			except ValueError:	
				print(not_int) #Lets the user know they entered non-int
		
		print('\n')
	elif mode.lower() == 'medium':
		
		int_to_guess = randint(1,100) #Generates random int in range
		guesses = 0
		#Runs while they haven't quit yet
		while thisgame == True:
			
			
			guessed_int = input(medium_prompt)
			
			#Avoid crashing with try-except 
			try:
				if guessed_int == 'quit':
					break
					
				guessed_int = int(guessed_int) #casts input as int
				
				if guessed_int == int_to_guess:
					guesses+=1
					print(win)
					break
				elif guessed_int < 0 or guessed_int > 100:
					guesses+=1
					print(out_of_bounds)
				elif guessed_int < int_to_guess:
					guesses+=1
					print(low_guess)
				else:
					guesses+=1
					print(high_guess)
					
			except ValueError:	
				print(not_int)
		print(guessed_to_win+str(guesses)+"\n")
	elif mode.lower() == 'hard':
		
		int_to_guess = randint(1,1000) #Generates random int in range
		guesses = 0
		#Runs while they haven't quit yet
		while thisgame == True:
			
			
			guessed_int = input(hard_prompt)
			
			#Avoid crashing with try-except 
			try:
				if guessed_int == 'quit':
					break
					
				guessed_int = int(guessed_int) #casts input as int
				
				if guessed_int == int_to_guess:
					guesses+=1
					print(win)
					break
				elif guessed_int < 0 or guessed_int > 1000:
					guesses+=1
					print(out_of_bounds)
				elif guessed_int < int_to_guess:
					guesses+=1
					print(low_guess)
				else:
					guesses+=1
					print(high_guess)
					
			except ValueError:	
				print(not_int)
				
		print(guessed_to_win+str(guesses)+"\n")	
	elif mode.lower() == 'quit':
		thegame = False
		break
	else:
		print(invalid_mode)
		 
print('\nThanks for playing!')
