#Daniel Taylor, 16 January 2017
#Creates a list of pizza toppings, copies list, adds toppings, removes 
#topping from friends list, loops generate output to standard output 

#Create a list of pizza toppings you like
my_toppings=['SPAM','BACON_SPAM','JALEPENO_SPAM','LOWSODIUM_SPAM','SPAM_SPAM']

#copy the list to your friend
friends_toppings=list(my_toppings)

#Edit your list to include more toppings
my_toppings.append('FORGOTTEN_SPAM')
my_toppings.append('PROTO_SPAM')

#Remove a topping from your friends list
friends_toppings.pop()

#Add a new topping to your friends list
friends_toppings.append('pineapple')

#Print each list in its own loop
for p in my_toppings:
	print("I decided I like: "+p)
	
for f in friends_toppings:
	print("My friend has decided they like: "+ f)
