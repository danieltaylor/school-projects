#Dan Taylor
#CSIS 4850 
#6 Feb 2017
#Exercise 7.8, Deli


#List of sandwich orders.
sandwich_orders = ['Reuben', 'Spam', 'SpamnSpam', 'The Final Sandwich']

#Empty list
finished_sandwiches = []


#Loop to go through orders. 
for sandwich in sandwich_orders:
	print("I made your %s." %sandwich)
	finished_sandwiches.append(sandwich)
	del sandwich

print("We made all these sandwiches:")
print(finished_sandwiches)
