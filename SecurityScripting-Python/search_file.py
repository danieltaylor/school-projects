#Dan Taylor

file_name - 'hello.txt'

with open(file_name) as f:
	if 'Goodbye' in f.read():
		print('Found!')
	else:
		print('Not found')
