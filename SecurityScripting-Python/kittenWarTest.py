from anonBrowser import *

#Initializes ab, an anonBrowser, with the parameters passed
ab = anonBrowser(proxies=[], user_agents=[('User-agent',\
	'superSecretBrowser')])

#Clears ab's cookies, changes ab's user agent, reconnects
# kittenwar.com, prints out the cookies kittenwar gave ab
for attempt in range(1,5):
    ab.anonymize()
    print('[*] Fetching page')
    response = ab.open('http://kittenwar.com')
    
    for cookie in ab.cookie_jar:
        print cookie
