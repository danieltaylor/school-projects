#Dan Taylor
#5 April 2017
#Socket Test


import socket, sys, optparse

parsearooni = optparse.OptionParser()

parsearooni.add_option("-s", "--server_address", default = "localhost",
	dest = "server_address", help = "Connect to a server at an address",)
	
parsearooni.add_option("-p", "--p", default = 4444, dest = "port", 
	help = "Connect to server at this port. Default is 4444")

parsearooni.add_option("-v", "--v", default = False, dest = "verbose",
	help = "verbose")
	
parsearooni.add_option("-m", "--message", default = "", dest = "msg", 
	help = "This is the message you will send")

(options, args) = parsearooni.parse_args()

#if(options.verbose):
	

#Create a socket with ipv4, STREAM until connection terminated.
socker = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

#Defines, then connects to server test	
web_server = (options.server_address,options.port)
socker.connect(web_server)
print("Connected to: " + str(web_server[0]) + 
			" port: " + str(web_server[1]))

try:
	while True:
		
		#Prints connection info, prompts for messages
		mesg = str.encode(options.msg)
		socker.sendall(mesg)
		
		#Prints replies from server
		data = socker.recv(1024)
		data = data.decode()
		print(str(data))
		
finally:
	socker.close()

