#Dan Taylor
#CSIS 4850 
#6 Feb 2017
#Exercise 7.4, Pizza Toppings


#Creates prompt for user, kept within char limit
prompt = "Please enter a pizza topping to add to your pizza. You will "
prompt += "be prompted for more toppings until you enter 'quit'"


#Following code will run until user enters 'quit'
stringy = "barf"
while stringy != "quit":
	try:
		stringy = input(prompt)
		
		#Code to quit if user enters quit.
		if stringy == "quit":
			print("\nThanks. Please come again!")
			break
			
			
		#Prints unless they enter quit. 	
		print("\tThe customer is always right. We'll add %s to your pizza." %stringy)
		
	except ValueError:
		print("We expect pizza toppings.")
		
	
