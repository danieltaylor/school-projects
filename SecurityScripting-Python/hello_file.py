#Dan Taylor
# 6 March 2017

#Assigned as a variable so that we only have to change it in one place.
file_name = 'hello.txt' 

#with open(file_name) as f:
	#Assign the contents to a variable called contents.
#	contents = f.read()
#	print(contents)
	
# What happened? f.read reads the contents as a giant string. 

#with open(file_name) as f:
	#For loop, for reach line in f we print
	#for line in f:
	#	print(line)
		#Before, we had one big string with new lines. Now we have have
		#each line has its own print statement.
	
#with open(file_name) as f:
	#lines = f.readlines()
	#for line in lines:
		#print(lines)
	#Notice that what we have is a list. 
	#If you're searching for things, or matching, use strip to remove
	#whitespace from your list of results. 
	
with open(file_name) as f:
	lines = f.readlines()
	print(lines)
	for line in lines:
		if line.strip() == 'Hello World':
			print(line)
	#Notice that what we have is a list. 
	#If you're searching for things, or matching, use strip to remove
	#whitespace from your list of results. 
	#In this example, the 'Hello World' has an extra character \n. 
	#After removing it, we get the proper result. 
