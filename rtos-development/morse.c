
// reference https://forum.arduino.cc/index.php?topic=288234.0

// international morse code
int dot = 150;
int dash = dot*3;
int rest = dot;
int space = dot*3;
int wordspace = dot*7;

// letters
char a[3] = ".-";
char b[5] = "-...";
char c[5] = "-.-.";
char d[4] = "-..";
char e[2] = ".";
char f[5] = "..-.";
char g[5] = "--.";
char h[5] = "....";
char i[3] = "..";
char j[5] = ".---";
char k[4] = "-.-";
char l[5] = ".-..";
char m[3] = "--";
char n[3] = "-.";
char o[4] = "---";
char p[5] = ".--.";
char q[5] = "--.-";
char r[4] = ".-.";
char s[4] = "...";
char t[2] = "-";
char u[4] = "..-";
char v[5] = "...-";
char w[4] = ".--";
char x[5] = "-..-";
char y[5] = "-.--";
char z[5] = "--..";

const byte numChars = 32;
char receivedChars[numChars]; // an array to store the received data

boolean newData = false;

void recvWithEndMarker() {
  static byte ndx = 0;
  char endMarker = '\n';
  char rc;
 
  while (Serial.available() > 0 && newData == false) {
    rc = Serial.read();

    if (rc != endMarker) {
      receivedChars[ndx] = rc;
      ndx++;
      if (ndx >= numChars) {
        ndx = numChars - 1;
      }
    }
    else {
      receivedChars[ndx] = '\0'; // terminate the string
      ndx = 0;
      newData = true;
    } 
  }
  
}

void showNewData() {
  if (newData == true) {
    Serial.print("This just in ... ");
    Serial.println(receivedChars);
    newData = false;
  }
}

void setup() {
  // put your setup code here, to run once:
  // initialize digital pin LED_BUILTIN as an output.
  pinMode(LED_BUILTIN, OUTPUT);
  Serial.begin(9600);
  Serial.println("<Arduino is ready>");
}

void dit(){
  digitalWrite(LED_BUILTIN, HIGH);
  delay(dot);
  digitalWrite(LED_BUILTIN, LOW);
  delay(rest);
}

void dah(){
  digitalWrite(LED_BUILTIN, HIGH);
  delay(dash);
  digitalWrite(LED_BUILTIN, LOW);
  delay(rest);
}

void morse(char letter){
  switch (letter) {
    case 'a':
      morse_b(a);
      break;
    case 'b':
      morse_b(b);
      break;
    case 'c':
      morse_b(c);
      break;
    case 'd':
      morse_b(d);
      break;  
    case 'e':
      morse_b(e);
      break;  
    case 'f':
      morse_b(f);
      break;
    case 'g':
      morse_b(g);
      break;
    case 'h':
      morse_b(h);
      break;
    case 'i':
      morse_b(i);
      break;
    case 'j':
      morse_b(j);
      break;
    case 'k':
      morse_b(k);
      break;
    case 'l':
      morse_b(l);
      break;
    case 'm':
      morse_b(m);
      break;
    case 'n':
      morse_b(n);
      break;
    case 'o':
      morse_b(o);
      break;
    case 'p':
      morse_b(p);
      break;
    case 'q':
      morse_b(q);
      break;
    case 'r':
      morse_b(r);
      break;
    case 's':
      morse_b(s);
      break;
    case 't':
      morse_b(t);
      break;
    case 'u':
      morse_b(u);
      break;
    case 'v':
      morse_b(v);
      break;
    case 'w':
      morse_b(w);
      break;
    case 'x':
      morse_b(x);
      break;
    case 'y':
      morse_b(y);
      break;
    case 'z':
      morse_b(z);
      break;
  }
  delay(space);
}

void morse_b(char pieces[]){
  int c_length = sizeof(pieces)/sizeof(pieces[0]);
  for( int index=0; index <= c_length; index++){
    char piece = pieces[index];
    switch (piece){
      case '.':
        dit();
        break;
      case '-':
        dah();
        break;
    }
  }
}

void loop() {
  recvWithEndMarker();
   
  showNewData();
  
  int len = sizeof(receivedChars)/sizeof(receivedChars[0]);
  for( int index=0; index<=len; index++){
    morse(receivedChars[index]);
  }
  delay(wordspace);
 
  
}